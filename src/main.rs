use clap::{Parser, Subcommand};
use env_logger;

use gudr::command_handlers;

#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
struct Args {
    #[clap(subcommand)]
    command: Option<Command>,
}

#[derive(Subcommand)]
enum Command {
    Create {
        #[clap()]
        path: String,
    },
    Info,
    DumpDb,
    StoreTree {
        #[clap()]
        path: String,
    },
    CommitTree {
        #[clap()]
        hash: String,
        #[clap()]
        description: String,
        #[clap()]
        parent: String,
    },
    SetBranchHead {
        #[clap()]
        branch: String,
        #[clap()]
        commit_hash: String,
    },
    Log,
    Diff {
        #[clap()]
        ref_name1: String,
        #[clap()]
        ref_name2: String,
    },
    Checkout {
        ref_name: String,
    },
    Commit {
        description: String,
    },
    Tag {
        name: String,
        commit: String,
    },
    Branch {
        name: String,
    },
    Status {
        #[clap(short='d', long="diff")]
        with_diff: bool,
    },
    Merge {
        ref_name: String, 
    },
    Fetch {
        other_repo_path: String,
    }
}

fn main() -> Result<(), String> {
    env_logger::init();
    let args = Args::parse();
    let handler: Box<dyn command_handlers::CommandHandler> = 
    match &args.command {
        Some(Command::Create{path}) => {
            Box::new(command_handlers::HandleCreate{path: path})
        }
        Some(Command::Info) => {
            Box::new(command_handlers::HandleInfo{})
        }
        Some(Command::DumpDb) => {
            Box::new(command_handlers::HandleDumpDb{})
        }
        Some(Command::StoreTree{path}) => {
            Box::new(command_handlers::HandleStoreTree{path: path})
        }
        Some(Command::CommitTree{hash, description, parent}) => {
            Box::new(command_handlers::HandleCreateCommit{tree_hash: hash, description: description, parent: parent})
        }
        Some(Command::SetBranchHead{branch, commit_hash}) => {
            Box::new(command_handlers::HandleSetBranchHead{branch:branch, commit_hash: commit_hash})
        }
        Some(Command::Log) => {
            Box::new(command_handlers::HandleLog{})
        }
        Some(Command::Diff{ref_name1, ref_name2}) => {
            Box::new(command_handlers::HandleDiff{ref_name1, ref_name2})
        }
        Some(Command::Checkout{ref_name}) => {
            Box::new(command_handlers::HandleCheckout{ref_name})
        }
        Some(Command::Commit{description}) => {
            Box::new(command_handlers::HandleCommit{description})
        }
        Some(Command::Tag{name, commit}) => {
            Box::new(command_handlers::HandleTag{name, commit})
        }
        Some(Command::Branch{name}) => {
            Box::new(command_handlers::HandleBranch{name})
        }
        Some(Command::Status{with_diff}) => {
            Box::new(command_handlers::HandleStatus{with_diff: *with_diff})
        }
        Some(Command::Merge{ref_name}) => {
            Box::new(command_handlers::HandleMerge{ref_name})
        }
        Some(Command::Fetch{other_repo_path}) => {
            Box::new(command_handlers::HandleFetch{other_repo_path})
        }
        None => {
            Box::new(command_handlers::HandleNoCommand{})
        }
    };
    let output = handler.handle_command()?;
    println!("{}", output);
    return Ok(());
}
