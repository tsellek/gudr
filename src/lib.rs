mod repo;
mod db;
mod direntry;
mod config;
mod consts;
mod filesystem;
mod tree;
mod util;
mod hash;


pub mod command_handlers{

    use std::fs;
    use std::path;
    use crate::repo::{RemoteRepo, fs::FsRepo};
    use crate::consts;
    use crate::hash;

    pub trait CommandHandler {
        fn handle_command(&self) -> Result<String, String>;
    }

    pub struct HandleNoCommand {
    }

    pub struct HandleCreate<'a> {
        pub path: &'a String,
    }

    pub struct HandleInfo {
    }

    pub struct HandleDumpDb {
    }

    pub struct HandleStoreTree<'a> {
        pub path: &'a String,
    }

    pub struct HandleCreateCommit<'a> {
        pub tree_hash: &'a String,
        pub description: &'a String,
        pub parent: &'a String,
    }

    pub struct HandleLog {
    }

    pub struct HandleSetBranchHead<'a> {
        pub branch: &'a String,
        pub commit_hash: &'a String,
    }

    pub struct HandleDiff<'a> {
        pub ref_name1: &'a String,
        pub ref_name2: &'a String,
    }

    pub struct HandleCheckout<'a> {
        pub ref_name: &'a String,
    }

    pub struct HandleCommit<'a> {
        pub description: &'a String,
    }

    pub struct HandleTag<'a> {
        pub name: &'a String,
        pub commit: &'a String,
    }

    pub struct HandleBranch<'a> {
        pub name: &'a String,
    }

    pub struct HandleStatus {
        pub with_diff: bool,
    }

    pub struct HandleMerge<'a> {
        pub ref_name: &'a String,
    }

    pub struct HandleFetch<'a> {
        pub other_repo_path: &'a String,
    }

    impl CommandHandler for HandleNoCommand {
        fn handle_command(&self) -> Result<String, String>{
            return Err(String::from("No command provided"))
        }
    }

    impl<'a> CommandHandler for HandleCreate<'a> {
        fn handle_command(&self) -> Result<String, String>{
            let mut gudr_path = path::PathBuf::from(self.path);
            gudr_path.push(".gudr");
            if let Err(err) = fs::create_dir_all(gudr_path) {
                return Err(err.to_string());
            }
            FsRepo::new(path::Path::new(&self.path))?;
            Ok(String::new())
        }
    }

    impl CommandHandler for HandleInfo {
        fn handle_command(&self) -> Result<String, String>{
            let repo = FsRepo::new(path::Path::new("."));
            repo?.info()
        }
    }    
    
    impl CommandHandler for HandleDumpDb {
        fn handle_command(&self) -> Result<String, String>{
            let repo = FsRepo::new(path::Path::new("."));
            repo?.dump()
        }
    }


    impl<'a> CommandHandler for HandleStoreTree<'a> {
        fn handle_command(&self) -> Result<String, String>{
            let mut repo = FsRepo::new(path::Path::new(&self.path))?;
            repo.fs_tree_store(path::Path::new(self.path))
        }
    }

    impl<'a> CommandHandler for HandleCreateCommit<'a> {
        fn handle_command(&self) -> Result<String, String>{
            let mut repo = FsRepo::new(path::Path::new("."))?;
            repo.commit_create(self.description, &repo.current_user(), self.tree_hash, self.parent, None)
        }
    }

    impl<'a> CommandHandler for HandleLog {
        fn handle_command(&self) -> Result<String, String>{
            let repo = FsRepo::new(path::Path::new("."))?;
            let cur_commit_hash = repo.branch_get_head(&repo.current_branch())?;
            if let None = cur_commit_hash {
                return Ok(String::new());
            }

            let commit_ancestors = repo.commit_list_ancestors(&cur_commit_hash.unwrap(), None)?;

            let mut output = String::new();
            for ancestor in commit_ancestors {
                output.push_str(&format!("\n\n{}", ancestor));
            }

            Ok(output)
        }
    }

    impl<'a> CommandHandler for HandleSetBranchHead<'a> {
        fn handle_command(&self) -> Result<String, String>{
            let mut repo = FsRepo::new(path::Path::new("."))?;
            repo.branch_set_head(self.branch, self.commit_hash)
        }
    }


    impl<'a> CommandHandler for HandleDiff<'a> {
        fn handle_command(&self) -> Result<String, String>{
            let repo = FsRepo::new(path::Path::new("."))?;
            let commit1 = repo.resolve_ref(self.ref_name1)?;
            let commit2 = repo.resolve_ref(self.ref_name2)?;
            repo.commit_diff(commit_hash_from_ref(&commit1)?, commit_hash_from_ref(&commit2)?)
        }
    }

    impl<'a> CommandHandler for HandleCheckout<'a> {
        fn handle_command(&self) -> Result<String, String>{
            let mut repo = FsRepo::new(path::Path::new("."))?;
            let resolved_ref = repo.resolve_ref(self.ref_name)?;
            match resolved_ref {
                consts::ResolvedRef::Branch(consts::RefInfo{name:_, origin:_, commit: Some(commit_hash)}) => {
                    let result = repo.checkout(&commit_hash);
                    repo.current_branch_set(self.ref_name)?;
                    return result;
                }
                consts::ResolvedRef::Commit(consts::RefInfo{name:_, origin:_, commit: Some(commit_hash)}) | consts::ResolvedRef::Tag(consts::RefInfo{name:_, origin:_, commit: Some(commit_hash)}) => {
                    let result = repo.checkout(&commit_hash);
                    repo.current_commit_set(&commit_hash)?;
                    return result;
                }
                consts::ResolvedRef::Branch(consts::RefInfo{name: branch_name, origin:_, commit: None}) => {
                    return Err(format!("Branch '{}' has no commits yet", branch_name));
                }
                consts::ResolvedRef::Tag(consts::RefInfo{name: _, origin:_, commit: None}) | consts::ResolvedRef::Commit(consts::RefInfo{name: _, origin:_, commit: None})=> {
                    panic!("Tags and commit refs should always reference a commit");
                }
            }
        }
    }

    impl<'a> CommandHandler for HandleCommit<'a> {
        fn handle_command(&self) -> Result<String, String>{
            let mut repo = FsRepo::new(path::Path::new("."))?;
            commit_fs_to_current_branch(&mut repo, self.description, None)
        }
    }

    impl<'a> CommandHandler for HandleTag<'a> {
        fn handle_command(&self) -> Result<String, String>{
            let mut repo = FsRepo::new(path::Path::new("."))?;
            repo.tag_set(self.name, self.commit)?;
            return Ok(String::new());
        }
    }

    impl<'a> CommandHandler for HandleBranch<'a> {
        fn handle_command(&self) -> Result<String, String>{
            let mut repo = FsRepo::new(path::Path::new("."))?;
            repo.branch_create(self.name)?;
            return Ok(String::new());
        }
    }

    impl CommandHandler for HandleStatus {
        fn handle_command(&self) -> Result<String, String>{
            let repo = FsRepo::new(path::Path::new("."))?;
            repo.status(self.with_diff)
        }
    }

    impl<'a> CommandHandler for HandleMerge<'a> {
        fn handle_command(&self) -> Result<String, String>{
            let mut repo = FsRepo::new(path::Path::new("."))?;
            let resolved_ref = repo.resolve_ref(self.ref_name)?;
            let commit_to_merge = commit_hash_from_ref(&resolved_ref)?;
            let conflicts = repo.merge(commit_to_merge)?;
            if !conflicts.is_empty() {
                Ok(conflicts)
            } else {
                commit_fs_to_current_branch(&mut repo, &consts::merge_commit_description(self.ref_name, commit_to_merge), Some(commit_to_merge))
            }
        }
    }

    impl<'a> CommandHandler for HandleFetch<'a> {
        fn handle_command(&self) -> Result<String, String>{
            let mut local = FsRepo::new(path::Path::new("."))?;
            let remote = FsRepo::new(path::Path::new(self.other_repo_path))?;

            let pull_results = local.fetch(&remote, &self.other_repo_path)?;

            let mut output = String::new();

            for result in pull_results {
                match result {
                    consts::ResolvedRef::Branch(ref_info) => {
                        output.push_str(&format!("Branch '{}'\n", ref_info.name));
                    }
                    consts::ResolvedRef::Tag(ref_info) => {
                        output.push_str(&format!("Tag '{}'\n", ref_info.name));
                    }                    
                    _ => {
                        panic!("Unexpected pulled ref")
                    }
                }
            }

            Ok(output)
        }
    }

    fn commit_hash_from_ref(r: &consts::ResolvedRef) -> Result<&hash::OwnedEncodedHash, String> {
        match r {
            consts::ResolvedRef::Branch(consts::RefInfo{name:_, origin:_, commit: Some(commit_hash)})|
            consts::ResolvedRef::Tag(consts::RefInfo{name:_, origin:_, commit: Some(commit_hash)})|
            consts::ResolvedRef::Commit(consts::RefInfo{name:_, origin:_, commit: Some(commit_hash)}) => {
                Ok(commit_hash)
            }
            consts::ResolvedRef::Branch(consts::RefInfo{name: branch_name, origin:_, commit: None}) =>{
                Err(format!("Branch '{}' has no commits yet", branch_name))
            }
            consts::ResolvedRef::Tag(consts::RefInfo{name:_, origin:_, commit: None}) | 
            consts::ResolvedRef::Commit(consts::RefInfo{name:_, origin:_, commit: None})
            => {
                panic!("Tags and commits should always reference a commit");
            }
        }
    }

    fn commit_fs_to_current_branch(repo: &mut FsRepo, description: &str, merged_commit: Option<hash::EncodedHash>) -> Result<String, String> {
        let current_branch_head = repo.branch_get_head(repo.current_branch())?;
        let current_branch_head_encoded = if let Some(branch_head) = current_branch_head {branch_head} else {String::new()};

        let tree_hash = repo.fs_tree_store(path::Path::new("."))?;
        let commit_hash = repo.commit_create(description, &repo.current_user(), &tree_hash, &current_branch_head_encoded, merged_commit)?;
        let current_branch = repo.current_branch().clone();
        repo.branch_set_head(&current_branch, &commit_hash)?;
        repo.current_commit_set(&commit_hash)?;
        return Ok(commit_hash);
    }
}

#[cfg(test)]
mod run_tests{
    // These tests set the process's working directory and thus do not support multithreading. Run them with `cargo test -- --test-threads 1`
    use std::{fs, path};
    use std::io::Write;

    use insta;
    use ::function_name::named;

    use crate::consts;
    use crate::command_handlers::*;
    use crate::util::test;
    use crate::repo::fs::FsRepo;

    #[named]
    #[test]
    fn test_create() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());

        insta::assert_debug_snapshot!(HandleInfo{}.handle_command().unwrap());
        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_store_tree() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());

        insta::assert_debug_snapshot!(HandleStoreTree{path: &".".to_owned()}.handle_command().unwrap());
        insta::assert_debug_snapshot!(HandleDumpDb{}.handle_command().unwrap());
        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_store_tree_empty_dir() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());

        fs::create_dir("some_dir").unwrap();

        insta::assert_debug_snapshot!(HandleStoreTree{path: &".".to_owned()}.handle_command().unwrap());
        insta::assert_debug_snapshot!(HandleDumpDb{}.handle_command().unwrap());
        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_create_commit() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());

        let tree_hash = HandleStoreTree{path: &".".to_owned()}.handle_command().unwrap();
        insta::assert_debug_snapshot!(HandleCreateCommit{tree_hash: &tree_hash, description: &"commit description".to_owned(), parent: &"".to_owned()}.handle_command().unwrap());
        insta::assert_debug_snapshot!(HandleDumpDb{}.handle_command().unwrap());

        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_create_commit_unchanged_parent() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());

        fs::write("some_file.txt", "some_data").unwrap();
        fs::create_dir("some_dir").unwrap();
        fs::write("some_dir/some_file.txt", "some_data").unwrap();
        commit_to_current_branch();

        fs::write("some_file.txt", "some_other_data").unwrap();
        commit_to_current_branch();

        insta::assert_debug_snapshot!(HandleDumpDb{}.handle_command().unwrap());

        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_log() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());

        commit_to_current_branch();
        insta::assert_debug_snapshot!(HandleLog{}.handle_command().unwrap());

        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_diff_same_commit() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());

        let commit_hash = commit_to_current_branch();
        insta::assert_debug_snapshot!(HandleDiff{ref_name1: &commit_hash, ref_name2: &commit_hash}.handle_command().unwrap());

        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_diff_single_file_added_modified() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());

        let commit_hash1 = commit_to_current_branch();
        fs::write("some_file.txt", "some_file_contents\n".as_bytes()).unwrap();
        let commit_hash2 = commit_to_current_branch();
        fs::write("some_file.txt", "some_modified_file_contents\n".as_bytes()).unwrap();
        let commit_hash3 = commit_to_current_branch();

        insta::assert_debug_snapshot!(HandleDiff{ref_name1: &commit_hash1, ref_name2: &commit_hash2}.handle_command().unwrap());
        insta::assert_debug_snapshot!(HandleDiff{ref_name1: &commit_hash2, ref_name2: &commit_hash3}.handle_command().unwrap());

        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_diff_single_file_deleted() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());

        fs::write("some_file.txt", "some_file_contents".as_bytes()).unwrap();
        let commit_hash1 = commit_to_current_branch();
        fs::remove_file("some_file.txt").unwrap();
        let commit_hash2 = commit_to_current_branch();

        insta::assert_debug_snapshot!(HandleDiff{ref_name1: &commit_hash1, ref_name2: &commit_hash2}.handle_command().unwrap());

        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_diff_subdir() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());

        let commit_hash1 = commit_to_current_branch();
        fs::create_dir("subdir").unwrap();
        fs::write("subdir/some_file.txt", "some_file_contents\n".as_bytes()).unwrap();
        fs::write("subdir/some_file1.txt", "blah".as_bytes()).unwrap();
        let commit_hash2 = commit_to_current_branch();
        fs::write("subdir/some_file.txt", "modified_file_contents\n".as_bytes()).unwrap();
        fs::write("subdir/some_file2.txt", "modified_file_contents\n".as_bytes()).unwrap();
        fs::remove_file("subdir/some_file1.txt").unwrap();
        let commit_hash3 = commit_to_current_branch();

        insta::assert_debug_snapshot!(HandleDiff{ref_name1: &commit_hash1, ref_name2: &commit_hash2}.handle_command().unwrap());
        insta::assert_debug_snapshot!(HandleDiff{ref_name1: &commit_hash2, ref_name2: &commit_hash3}.handle_command().unwrap());

        std::env::set_current_dir(&cwd).unwrap();
    }


    #[named]
    #[test]
    fn test_checkout_single_file_deleted_in_commit_added_in_fs() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());

        let commit_hash = commit_to_current_branch();
        fs::write("some_file.txt", "some_file_contents".as_bytes()).unwrap();
        commit_to_current_branch();
        fs::write("some_file.txt", "some_other_file_contents".as_bytes()).unwrap();

        let checkout_result = HandleCheckout{ref_name: &commit_hash}.handle_command();

        insta::assert_debug_snapshot!(test::expect_error(checkout_result));

        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_checkout_single_file_added_in_commit_added_in_fs() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());

        commit_to_current_branch();
        fs::write("some_file.txt", "some_file_contents".as_bytes()).unwrap();
        fs::write("some_file2.txt", "some_file_contents2".as_bytes()).unwrap();
        let commit_hash = commit_to_current_branch();
        fs::remove_file("some_file.txt").unwrap();
        commit_to_current_branch();
        fs::write("some_file.txt", "some_other_file_contents".as_bytes()).unwrap();

        let checkout_result = HandleCheckout{ref_name: &commit_hash}.handle_command();

        insta::assert_debug_snapshot!(test::expect_error(checkout_result));

        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_checkout_single_file_modified_in_commit_deleted_in_fs() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());

        commit_to_current_branch();
        fs::write("some_file.txt", "some_file_contents".as_bytes()).unwrap();
        let commit_hash = commit_to_current_branch();
        fs::write("some_file.txt", "some_other_file_contents".as_bytes()).unwrap();
        commit_to_current_branch();
        fs::remove_file("some_file.txt").unwrap();

        let checkout_result = HandleCheckout{ref_name: &commit_hash}.handle_command();

        insta::assert_debug_snapshot!(test::expect_error(checkout_result));

        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_checkout_single_file_modified_in_commit_modified_in_fs() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());

        commit_to_current_branch();
        fs::write("some_file.txt", "some_file_contents".as_bytes()).unwrap();
        let commit_hash = commit_to_current_branch();
        fs::write("some_file.txt", "some_other_file_contents".as_bytes()).unwrap();
        commit_to_current_branch();
        fs::write("some_file.txt", "yet_some_other_file_contents".as_bytes()).unwrap();

        let checkout_result = HandleCheckout{ref_name: &commit_hash}.handle_command();

        insta::assert_debug_snapshot!(test::expect_error(checkout_result));

        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_checkout_single_file() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());

        let commit_hash1 = commit_to_current_branch();
        fs::write("some_file.txt", "some_file_contents".as_bytes()).unwrap();
        let commit_hash2 = commit_to_current_branch();

        insta::assert_debug_snapshot!(HandleCheckout{ref_name: &commit_hash1}.handle_command().unwrap());
        assert_eq!(false, path::Path::new("some_file.txt").exists());
        insta::assert_debug_snapshot!(HandleCheckout{ref_name: &commit_hash2}.handle_command().unwrap());
        assert_eq!(fs::read_to_string("some_file.txt").unwrap(), "some_file_contents");

        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_checkout_single_file_deleted_in_commit() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());

        let commit_hash = commit_to_current_branch();
        fs::write("some_file.txt", "some_file_contents".as_bytes()).unwrap();
        commit_to_current_branch();

        let checkout_result = HandleCheckout{ref_name: &commit_hash}.handle_command();

        assert!(!path::Path::new("some_file.txt").exists());

        insta::assert_debug_snapshot!(checkout_result);

        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_checkout_no_current_commit() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());

        fs::write("some_file.txt", "some_file_contents".as_bytes()).unwrap();
        let commit_hash = commit_to_current_branch();

        fs::remove_file(".gudr/config.json").unwrap();
        fs::remove_file("some_file.txt").unwrap();

        insta::assert_debug_snapshot!(HandleCheckout{ref_name: &commit_hash}.handle_command().unwrap());
        insta::assert_debug_snapshot!(fs::read("some_file.txt").unwrap());
        insta::assert_debug_snapshot!(HandleInfo{}.handle_command().unwrap());

        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_checkout_large_file() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());

        let file_contents = [0; 1<<20];

        let commit_hash = commit_to_current_branch();
        fs::write("some_file.txt", file_contents).unwrap();
        let commit_hash2 = commit_to_current_branch();

        HandleCheckout{ref_name: &commit_hash}.handle_command().unwrap();

        assert!(!path::Path::new("some_file.txt").exists());

        HandleCheckout{ref_name: &commit_hash2}.handle_command().unwrap();

        assert_eq!(std::fs::read("some_file.txt").unwrap(), file_contents);

        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_ignore() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());

        fs::write("some_file.txt", "some_file_contents".as_bytes()).unwrap();
        fs::write("some_other_file.txt", "some_file_contents".as_bytes()).unwrap();
        fs::create_dir_all("subdir1").unwrap();
        fs::create_dir_all("subdir2").unwrap();
        fs::write("subdir1/some_other_file.txt", "blah1".as_bytes()).unwrap();
        fs::write("subdir2/some_other_file.txt", "blah2".as_bytes()).unwrap();
        let mut ignorefile = fs::File::options().append(true).open(".gudr.ignore").unwrap();
        writeln!(ignorefile, "some_other*").unwrap();
        writeln!(ignorefile, "subdir1/").unwrap();
        ignorefile.flush().unwrap();
        
        commit_to_current_branch();

        let db_contents = HandleDumpDb{}.handle_command().unwrap();
        assert!(db_contents.find("| subdir1/").is_none());
        assert!(db_contents.find("| subdir1").is_some());
        assert!(db_contents.find("| some_other_file.txt").is_none());
        assert!(db_contents.find("| some_file.txt").is_some());
        assert!(db_contents.find("| subdir2/some_other_file.txt").is_some());

        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_resolve_tag() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());
        let commit1 = commit_to_current_branch();
        HandleTag{name: &String::from("test_tag"), commit: &commit1}.handle_command().unwrap();
        fs::write("some_file.txt", "some contents").unwrap();
        let commit2 = commit_to_current_branch();
        insta::assert_debug_snapshot!(HandleDiff{ref_name1: &String::from("test_tag"), ref_name2: &commit2}.handle_command().unwrap());

        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_resolve_branch() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());
        let commit1 = commit_to_current_branch();
        fs::write("some_file.txt", "some contents").unwrap();
        commit_to_current_branch();
        insta::assert_debug_snapshot!(HandleDiff{ref_name1: &commit1, ref_name2: &String::from("trunk")}.handle_command().unwrap());

        std::env::set_current_dir(&cwd).unwrap();
    }


    #[named]
    #[test]
    fn test_resolve_partial() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());
        let commit1 = commit_to_current_branch();
        fs::write("some_file.txt", "some contents").unwrap();
        let commit2 = commit_to_current_branch();
        insta::assert_debug_snapshot!(HandleDiff{ref_name1: &commit1, ref_name2: &commit2[..6].to_owned()}.handle_command().unwrap());

        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_create_branch_and_commit_to_it() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());

        commit_to_current_branch();
        
        HandleBranch{name: &String::from("branch")}.handle_command().unwrap();

        fs::write("some_file.txt", "some contents").unwrap();
        commit_to_current_branch();
        
        insta::assert_debug_snapshot!(HandleDiff{ref_name1: &String::from(consts::DEFAULT_BRANCH), ref_name2: &String::from("branch")}.handle_command().unwrap());

        insta::assert_debug_snapshot!(HandleCheckout{ref_name: &String::from("branch")}.handle_command().unwrap());
        assert!(!path::Path::new("some_file.txt").exists());

        insta::assert_debug_snapshot!(HandleCheckout{ref_name: &String::from(consts::DEFAULT_BRANCH)}.handle_command().unwrap());
        assert!(path::Path::new("some_file.txt").exists());

        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_status() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());

        fs::write("some_file1.txt", "some contents").unwrap();
        fs::write("some_file2.txt", "some contents").unwrap();
        commit_to_current_branch();
        fs::write("some_file3.txt", "some contents").unwrap();
        fs::remove_file("some_file2.txt").unwrap();
        fs::write("some_file2.txt", "some other contents").unwrap();

        insta::assert_debug_snapshot!(HandleStatus{with_diff: false}.handle_command().unwrap());

        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_status_with_diff() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());

        fs::write("some_file1.txt", "some contents").unwrap();
        fs::write("some_file2.txt", "some contents\n").unwrap();
        commit_to_current_branch();
        fs::write("some_file3.txt", "some contents").unwrap();
        fs::remove_file("some_file2.txt").unwrap();
        fs::write("some_file2.txt", "some other contents\n").unwrap();

        insta::assert_debug_snapshot!(HandleStatus{with_diff: true}.handle_command().unwrap());

        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_checkout_branch_sets_current_branch() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());
        
        commit_to_current_branch();

        HandleBranch{name: &"branch1".to_string()}.handle_command().unwrap();

        fs::write("some_file1.txt", "some contents").unwrap();
        commit_to_current_branch();
        assert_eq!("trunk", FsRepo::new(path::Path::new(".")).unwrap().current_branch());

        HandleCheckout{ref_name: &"branch1".to_string()}.handle_command().unwrap();
        assert_eq!("branch1", FsRepo::new(path::Path::new(".")).unwrap().current_branch());

        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_merge_file_added_in_other_branch() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());
        
        commit_to_current_branch();

        HandleBranch{name: &"branch2".to_string()}.handle_command().unwrap();
        HandleBranch{name: &"branch1".to_string()}.handle_command().unwrap();
        HandleCheckout{ref_name: &"branch1".to_string()}.handle_command().unwrap();

        fs::write("some_file1.txt", "some contents").unwrap();
        commit_to_current_branch();

        HandleCheckout{ref_name: &"branch2".to_string()}.handle_command().unwrap();
        assert!(!path::Path::new("some_file1.txt").exists());

        insta::assert_debug_snapshot!(HandleMerge{ref_name: &"branch1".to_string()}.handle_command().unwrap());
        insta::assert_debug_snapshot!(fs::read_to_string("some_file1.txt").unwrap());

        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_merge_different_files_added_in_both_branches() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());
        
        commit_to_current_branch();

        HandleBranch{name: &"branch2".to_string()}.handle_command().unwrap();
        HandleBranch{name: &"branch1".to_string()}.handle_command().unwrap();
        HandleCheckout{ref_name: &"branch1".to_string()}.handle_command().unwrap();

        fs::write("some_file1.txt", "some contents").unwrap();
        commit_to_current_branch();

        HandleCheckout{ref_name: &"branch2".to_string()}.handle_command().unwrap();
        assert!(!path::Path::new("some_file1.txt").exists());
        fs::write("some_file2.txt", "some contents2").unwrap();
        commit_to_current_branch();

        insta::assert_debug_snapshot!(HandleMerge{ref_name: &"branch1".to_string()}.handle_command().unwrap());
        insta::assert_debug_snapshot!(fs::read_to_string("some_file1.txt").unwrap());
        insta::assert_debug_snapshot!(fs::read_to_string("some_file2.txt").unwrap());

        std::env::set_current_dir(&cwd).unwrap();
    }


    #[named]
    #[test]
    fn test_merge_file_deleted_in_other_branch() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());
        
        fs::write("some_file1.txt", "some contents").unwrap();
        commit_to_current_branch();

        HandleBranch{name: &"branch2".to_string()}.handle_command().unwrap();
        HandleBranch{name: &"branch1".to_string()}.handle_command().unwrap();
        HandleCheckout{ref_name: &"branch1".to_string()}.handle_command().unwrap();

        fs::remove_file("some_file1.txt").unwrap();
        commit_to_current_branch();

        HandleCheckout{ref_name: &"branch2".to_string()}.handle_command().unwrap();
        assert!(path::Path::new("some_file1.txt").exists());

        insta::assert_debug_snapshot!(HandleMerge{ref_name: &"branch1".to_string()}.handle_command().unwrap());
        assert!(!path::Path::new("some_file1.txt").exists());

        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_merge_file_modified_in_other_branch() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());
        
        fs::write("some_file1.txt", "some contents").unwrap();
        commit_to_current_branch();

        HandleBranch{name: &"branch2".to_string()}.handle_command().unwrap();
        HandleBranch{name: &"branch1".to_string()}.handle_command().unwrap();
        HandleCheckout{ref_name: &"branch1".to_string()}.handle_command().unwrap();

        fs::write("some_file1.txt", "some other contents").unwrap();
        commit_to_current_branch();

        HandleCheckout{ref_name: &"branch2".to_string()}.handle_command().unwrap();
        assert_eq!("some contents".as_bytes(), fs::read("some_file1.txt").unwrap());

        insta::assert_debug_snapshot!(HandleMerge{ref_name: &"branch1".to_string()}.handle_command().unwrap());
        assert_eq!("some other contents".as_bytes(), fs::read("some_file1.txt").unwrap());

        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_merge_file_conflict() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());
        
        fs::write("some_file1.txt", "line1\nline2\nline3\nline4").unwrap();
        commit_to_current_branch();

        HandleBranch{name: &"branch2".to_string()}.handle_command().unwrap();
        HandleBranch{name: &"branch1".to_string()}.handle_command().unwrap();
        HandleCheckout{ref_name: &"branch1".to_string()}.handle_command().unwrap();

        fs::write("some_file1.txt", "line1\nline2_modified\nline3\nline4").unwrap();
        commit_to_current_branch();

        HandleCheckout{ref_name: &"branch2".to_string()}.handle_command().unwrap();
        fs::write("some_file1.txt", "line1\nline2\nline3_modified\nline4\nline5").unwrap();
        commit_to_current_branch();

        insta::assert_debug_snapshot!(HandleMerge{ref_name: &"branch1".to_string()}.handle_command().unwrap());
        insta::assert_debug_snapshot!(fs::read_to_string("some_file1.txt",).unwrap());

        std::env::set_current_dir(&cwd).unwrap();
    }

    #[named]
    #[test]
    fn test_merge_file_added_in_both_branches() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());
        
        commit_to_current_branch();

        HandleBranch{name: &"branch2".to_string()}.handle_command().unwrap();
        HandleBranch{name: &"branch1".to_string()}.handle_command().unwrap();
        HandleCheckout{ref_name: &"branch1".to_string()}.handle_command().unwrap();

        fs::write("some_file1.txt", "line1\nline2_modified\nline3\nline4").unwrap();
        commit_to_current_branch();

        HandleCheckout{ref_name: &"branch2".to_string()}.handle_command().unwrap();
        fs::write("some_file1.txt", "line1\nline2\nline3_modified\nline4\nline5").unwrap();
        commit_to_current_branch();

        insta::assert_debug_snapshot!(HandleMerge{ref_name: &"branch1".to_string()}.handle_command().unwrap());
        insta::assert_debug_snapshot!(fs::read_to_string("some_file1.txt",).unwrap());

        std::env::set_current_dir(&cwd).unwrap();
    }


    #[named]
    #[test]
    fn test_merge_path_ordering() {
        let cwd = create_test_repo_and_cd_to_it(function_name!());
        
        commit_to_current_branch();

        HandleBranch{name: &"branch2".to_string()}.handle_command().unwrap();
        HandleBranch{name: &"branch1".to_string()}.handle_command().unwrap();
        HandleCheckout{ref_name: &"branch1".to_string()}.handle_command().unwrap();

        fs::create_dir_all("a").unwrap();
        fs::create_dir_all("c/subdir").unwrap();
        fs::write("b_some_file1.txt", "toplevel").unwrap();
        fs::write("a/some_file1.txt", "a").unwrap();
        fs::write("c/some_file1.txt", "c").unwrap();
        fs::write("c/subdir/some_file1.txt", "c/subdir").unwrap();
        commit_to_current_branch();

        HandleCheckout{ref_name: &"branch2".to_string()}.handle_command().unwrap();
        fs::create_dir_all("b").unwrap();
        fs::create_dir_all("c/subdir2").unwrap();
        fs::write("b/some_file1.txt", "b").unwrap();
        fs::write("c/some_file1.txt", "c1").unwrap();
        fs::write("c/subdir2/some_file1.txt", "c/subdir2").unwrap();
        commit_to_current_branch();

        insta::assert_debug_snapshot!(HandleMerge{ref_name: &"branch1".to_string()}.handle_command().unwrap());
        insta::assert_debug_snapshot!(fs::read_to_string("a/some_file1.txt",).unwrap());
        insta::assert_debug_snapshot!(fs::read_to_string("c/subdir/some_file1.txt",).unwrap());
        insta::assert_debug_snapshot!(fs::read_to_string("c/some_file1.txt",).unwrap());

        std::env::set_current_dir(&cwd).unwrap();
    }

    fn commit_to_current_branch() -> String {
        HandleCommit{description: &"commit description".to_string()}.handle_command().unwrap()
    }
    
    fn create_test_repo_and_cd_to_it(test_name: &str) -> path::PathBuf {
        let path = test::prepare_test_dir(file!(), test_name);
        let cwd = std::env::current_dir().unwrap();
    
        HandleCreate{path: &path}.handle_command().unwrap();
        std::env::set_current_dir(&path).unwrap();
    
        return cwd;
    }
}
