use sha2::{self, Digest};

pub type OwnedRawHash = Vec<u8>;
pub type RawHash<'a> = &'a [u8];

pub type OwnedEncodedHash = String;
pub type EncodedHash<'a> = &'a str;

pub fn encode(raw_hash: RawHash) -> OwnedEncodedHash {
    hex::encode(raw_hash)
}

pub fn decode(encoded_hash: EncodedHash) -> Result<OwnedRawHash, String> {
    if let Ok(hex_str) = hex::decode(encoded_hash) {
        Ok(hex_str)
    } else {
        Err(format!("Invalid hash '{}'", encoded_hash))
    }
}

pub fn hash_bytes<T: AsRef<[u8]>>(strings: &[T]) -> OwnedRawHash{
    let mut hasher = sha2::Sha512::new();
    for s in strings {
        hasher.update(s.as_ref());
    }
    return hasher.finalize().as_slice().to_vec();
}


pub trait CloneDirEntryHasherIntoBox {
    fn clone_into_box(&self) -> Box<dyn DirEntryHasher>;
}

pub trait DirEntryHasher: CloneDirEntryHasherIntoBox {
    fn hash(&mut self, direntry: &crate::direntry::DirEntry) -> OwnedRawHash;
}

#[derive(Clone)]
pub struct ConstDirEntryHash {
    pub hash_value: OwnedRawHash
}

impl DirEntryHasher for ConstDirEntryHash {
    fn hash(&mut self, _direntry: &crate::direntry::DirEntry) -> OwnedRawHash {
        self.hash_value.clone()
    }
}

impl<T> CloneDirEntryHasherIntoBox for T
where
    T: DirEntryHasher + Clone + 'static,
{
    fn clone_into_box(&self) -> Box<dyn DirEntryHasher> {
        Box::new(self.clone())
    }
}

impl<'a> Clone for Box<dyn DirEntryHasher> {
    fn clone(&self) -> Self {
        self.clone_into_box()
    }
}
