use std::path;

use serde::{Serialize, Deserialize};

use crate::hash;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum DirEntryType {
    Dir = 1,
    File = 2,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DirEntryInfo{
    pub detype: DirEntryType,
    pub path: path::PathBuf,
    pub unix_permissions: u32,
}

pub trait FileContents{
    fn get(&self) -> std::rc::Rc<dyn AsRef<[u8]>>;
    fn create_copy<'a>(&self) -> Box<dyn FileContents>;
}

impl<'a> Clone for Box<dyn FileContents> {
    fn clone(&self) -> Box<dyn FileContents> {
        self.create_copy()
    }
}

pub struct DirEntry {
    hash: std::cell::RefCell<hash::OwnedRawHash>,
    hasher: std::cell::RefCell<Box<dyn hash::DirEntryHasher>>,
    pub info: DirEntryInfo,
    pub contents: Box<dyn FileContents>,
}

impl Clone for DirEntry {
    fn clone(&self) -> DirEntry{
        DirEntry::new(self.info.clone(), self.contents.create_copy(), self.hasher.borrow().clone())
    }
}

impl DirEntry {
    pub fn new(info: DirEntryInfo, contents: Box<dyn FileContents>, hasher: Box<dyn hash::DirEntryHasher>) -> DirEntry {
        DirEntry {
            hash: std::cell::RefCell::new(vec![]),
            hasher: std::cell::RefCell::new(hasher.clone_into_box()),
            info: info,
            contents: contents.create_copy(),
        }
    }

    pub fn get_hash(&self) -> hash::OwnedRawHash{
        if self.hash.borrow().is_empty() {
            self.hash.replace(self.hasher.borrow_mut().hash(self));
        }
        self.hash.borrow().clone()
    }
}

impl std::fmt::Debug for DirEntry {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("DirEntry")
        .field("hash", &hash::encode(&self.hash.borrow()))
        .field("info", &self.info)
        .field("contents", &Vec::from((*(*self.contents).get()).as_ref()))
        .finish()
    }
}
