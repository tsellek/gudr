use std::collections::HashMap;
use std::fs;
use std::path;
use std::os::unix::fs::MetadataExt;

use log::trace;

use crate::direntry::{DirEntry, DirEntryInfo, DirEntryType, FileContents};
use crate::hash;
use crate::util;

struct FsFileContents {
    path: path::PathBuf,
    contents: std::cell::RefCell<Option<std::rc::Rc<dyn AsRef<[u8]>>>>,
}

trait HashCache {
    fn get_hash(&mut self, full_path: &dyn AsRef<path::Path>, hash_path: &dyn AsRef<path::Path>) -> hash::OwnedRawHash;
    fn flush(&mut self, path: &dyn AsRef<path::Path>);
    fn flush_all(&mut self);
}

#[derive(Clone)]
struct FsDirEntryHasher {
    cache: std::rc::Rc<std::cell::RefCell<dyn HashCache>>,
    full_path: path::PathBuf,
}

impl hash::DirEntryHasher for FsDirEntryHasher {
    fn hash(&mut self, direntry: &DirEntry) -> hash::OwnedRawHash {
        self.cache.borrow_mut().get_hash(&self.full_path, &direntry.info.path)
    }
}

impl FileContents for FsFileContents {
    fn get(&self)-> std::rc::Rc<dyn AsRef<[u8]>> {
        if self.contents.borrow().is_some() {
            return self.contents.borrow().as_ref().unwrap().clone();
        }

        if self.path.is_file() {
            let file_contents = util::fs::file_contents(&self.path).unwrap();
            self.contents.replace(Some(file_contents.clone()));
            file_contents
        } else {
            let file_contents = std::rc::Rc::new(vec![]);
            self.contents.replace(Some(file_contents.clone()));
            file_contents
        }
    }

    fn create_copy(&self) -> Box<dyn FileContents> {
        Box::new(FsFileContents{
            path: self.path.clone(),
            contents: self.contents.clone(),
        })
    }
}

pub struct Filesystem {
    hash_cache: std::rc::Rc<std::cell::RefCell<HashMap<path::PathBuf, hash::OwnedRawHash>>>,
}

impl HashCache for HashMap<path::PathBuf, hash::OwnedRawHash> {
    fn get_hash(&mut self, full_path: &dyn AsRef<path::Path>, hash_path: &dyn AsRef<path::Path>) -> hash::OwnedRawHash {
        self.entry(full_path.as_ref().to_path_buf()).or_insert(Filesystem::do_file_hash(full_path, hash_path).unwrap()).clone()
    }

    fn flush(&mut self, path: &dyn AsRef<path::Path>) {
        self.remove(path.as_ref());
    }

    fn flush_all(&mut self) {
        self.clear();
    }
}

impl Filesystem {
    pub fn new() -> Filesystem {
        Filesystem{hash_cache: std::rc::Rc::new(std::cell::RefCell::new(HashMap::new()))}
    }
    pub fn read_file(&self, path: &dyn AsRef<path::Path>) -> Result<std::rc::Rc<dyn AsRef<[u8]>>, String> {
        util::fs::file_contents(path.as_ref())
    }

    pub fn write_file(&mut self, path: &dyn AsRef<path::Path>, data: &[u8]) -> Result<(), String> {
        util::fs::op(fs::write(path, data))
    }

    pub fn remove_file(&mut self, path: &dyn AsRef<path::Path>) -> Result<(), String> {
        util::fs::op(fs::remove_file(path))
    }

    pub fn file_hash(&self, path: &dyn AsRef<path::Path>) -> Result<hash::OwnedRawHash, String> {
        Ok(self.hash_cache.borrow_mut().get_hash(path, path).to_vec())
    }

    pub fn direntry_hash(&self, direntry: &DirEntry) -> Result<hash::OwnedRawHash, String> {
        Ok(util::fs::hash_file(&direntry.info, &*direntry.contents))
    }

    pub fn create_dir(&mut self, path: &dyn AsRef<path::Path>) -> Result<(), String> {
        util::fs::op(fs::create_dir(path))
    }

    pub fn read_direntry<'a>(&self, basedir: &dyn AsRef<path::Path>, path: &path::Path) -> Result<DirEntry, String> {
        trace!("Creating direntry: toplevel: '{}', path: '{}'", basedir.as_ref().to_string_lossy(), &path.to_string_lossy());
        assert!(path.exists(), "'{}' does not exist", path.to_string_lossy());
        let info = DirEntryInfo {
            detype: if path.is_dir() {DirEntryType::Dir} else {DirEntryType::File},
            path: path.strip_prefix(basedir).unwrap().to_path_buf(),
            unix_permissions: path.metadata().unwrap().mode(),
        };
        let contents = Box::new(FsFileContents{path: path::PathBuf::from(path), contents: std::cell::RefCell::new(None)});
        Ok(DirEntry::new(info, contents, Box::new(FsDirEntryHasher{cache: self.hash_cache.clone(), full_path: path.to_path_buf()})))
    }

    fn do_file_hash(full_path: &dyn AsRef<path::Path>, hash_path: &dyn AsRef<path::Path>) -> Result<hash::OwnedRawHash, String> {
        let actual_path = if full_path.as_ref().to_str().unwrap().is_empty() {&path::Path::new(".")} else {full_path.as_ref()};
        trace!("Hashing {:?}", actual_path);
        let contents = if actual_path.is_file() {util::fs::file_contents(actual_path)?} else {std::rc::Rc::new(vec![])};
        Ok(util::fs::hash_file_data(actual_path.metadata().unwrap().mode(), hash_path.as_ref().to_str().unwrap(), (*contents).as_ref()))
    }
}
