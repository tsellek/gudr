use crate::hash;

pub const DEFAULT_BRANCH: &str = "trunk";

pub const IGNORE_FILE: &str = ".gudr.ignore";

pub const DEFAULT_IGNORE_LIST: [&str; 1] = [r"\.gudr/"];

#[derive(Debug, PartialEq)]
pub enum RefType {
    Branch = 1,
    Tag = 2,
}

#[derive(Eq, Clone)]
pub struct Commit{
    pub hash: hash::OwnedEncodedHash,
    pub description: String,
    pub parent_hash: hash::OwnedEncodedHash,
    pub merged_parent_hash: Option<hash::OwnedEncodedHash>,
    pub user: String,
    pub tree_hash: hash::OwnedEncodedHash,
    pub timestamp: chrono::DateTime<chrono::offset::Utc>,
}

#[derive(Debug, PartialEq, Clone)]
pub struct RefInfo {
    pub name: String,
    pub origin: Option<String>,
    pub commit: Option<hash::OwnedEncodedHash>,
}

#[derive(Debug, PartialEq)]
pub enum ResolvedRef{
    Branch(RefInfo),
    Tag(RefInfo),
    Commit(RefInfo),
}

impl<'a> ResolvedRef {
    pub fn info(&'a self) -> &'a RefInfo {
        match self {
            ResolvedRef::Branch(ref_info) | ResolvedRef::Tag(ref_info) | ResolvedRef::Commit(ref_info) => {&ref_info}
        }
    }
}

pub fn merge_commit_description(ref_name: &str, commit_hash: hash::EncodedHash) -> String {
    if ref_name == commit_hash {
        format!("Merge {}", ref_name)
    } else {
        format!("Merge {} [{}]", ref_name, commit_hash)
    }
}

impl std::fmt::Debug for Commit {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Commit")
        .field("hash", &self.hash)
        .field("description", &self.description)
        .field("parent_hash", &self.parent_hash)
        .field("user", &self.user)
        .field("tree_hash", &self.tree_hash)
        .finish()
    }
}

impl std::fmt::Display for Commit {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{} [{}]\n{}", &self.hash[..6], &self.user, &self.description))
    }
}

impl std::cmp::PartialEq for Commit {
    fn eq(&self, other: &Self) -> bool {
        self.hash == other.hash
    }
}

impl std::hash::Hash for Commit {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.hash.hash(state);
    }
}