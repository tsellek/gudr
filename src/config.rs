use std::collections::HashSet;
use std::fs::File;
use std::io::Write;
use std::{fs, path};

use json;
use log::{trace, info};

use crate::consts;
use crate::hash;

pub struct Config {
    pub path: String,
    pub db_file_name: String,
    pub current_branch: String,
    pub current_checked_out_commit: Option<String>,
    pub ignore_list: HashSet<String>,
}

impl Config {
    pub fn new(path: &path::Path) -> Result<Config, String> {
        if Config::config_file_path(path).exists() {
            return Config::load(path);
        } else {
            return Config::create(path);
        }
    }

    pub fn info(&self) -> String {
        let mut ignorelist = Vec::from_iter(&self.ignore_list);
        ignorelist.sort_unstable();
        let cur_commit = if self.current_checked_out_commit.is_none() {"None".to_owned()} else {self.current_checked_out_commit.clone().unwrap()};
        return format!(
            "Repo Path:{}\nDB File: {}\nCurrent Branch: {}\nCurrent Commit:{:?}\nIgnoreList: [{}]{:?}\n",
            self.path,
            self.db_file_name,
            self.current_branch,
            &cur_commit,
            self.ignore_list.len(),
            ignorelist,
        );
    }

    pub fn save(&self) {
        let config_file_path = Config::config_file_path(path::Path::new(&self.path));
        info!(
            "Saving config file @'{}' (pwd={})",
            &config_file_path.to_string_lossy(),
            std::env::current_dir().unwrap().to_string_lossy()
        );
        let mut file = File::create(config_file_path).unwrap();
        let jsonobj = json::object! {
            "current_branch" => self.current_branch.clone(),
            "current_checked_out_commit" => self.current_checked_out_commit.clone(),
        };
        jsonobj.write(&mut file).unwrap();

        let mut ignorefile_path = path::PathBuf::from(&self.path);
        ignorefile_path.push(consts::IGNORE_FILE);
        if !self.ignore_list.is_empty() && !ignorefile_path.exists() {
            let mut file = File::create(ignorefile_path).unwrap();
            let mut ignorelist: Vec<String> = self.ignore_list.iter().map(|s|s[1..].to_owned()).collect();
            ignorelist.sort_unstable();
            for ignore_entry in ignorelist {
                file.write_all(ignore_entry.as_bytes()).unwrap();
                file.write_all(b"\n").unwrap();
            }
        }
    }

    pub fn current_user(&self) -> String {
        String::from("localuser")
    }

    pub fn current_branch(&self) -> &String {
        &self.current_branch
    }

    pub fn current_branch_set(&mut self, branch: &str) -> Result<(), String> {
        self.current_branch = branch.to_string();
        Ok(())
    }

    pub fn current_commit(&self) -> Option<&hash::OwnedEncodedHash> {
        if let Some(commit) = &self.current_checked_out_commit { Some(commit)} else {None}
    }

    pub fn current_commit_set(&mut self, encoded_commit_hash: &str) {
        self.current_checked_out_commit = Some(String::from(encoded_commit_hash));
    }

    pub fn should_ignore_path(&self, path: &path::PathBuf) -> bool {
        let path_str = path.to_string_lossy().into_owned();
        for ignore_entry in &self.ignore_list{
            if regex::Regex::new(ignore_entry).unwrap().is_match(&path_str) {
                trace!("'{}' matched against '{}'. Ignoring.", ignore_entry, path_str);
                return true;
            }
        }
        return self.ignore_list.contains(&path.to_string_lossy()[..]);
    }

    fn create(path: &path::Path) -> Result<Config, String> {
        return Ok(Config {
            path: String::from(path.to_str().unwrap()),
            db_file_name: String::from("db.sqlite3"),
            current_branch: String::from(consts::DEFAULT_BRANCH),
            current_checked_out_commit: None,
            ignore_list: HashSet::from_iter(
                consts::DEFAULT_IGNORE_LIST.iter().map(|s| format!("^{}", s)),
            ),
        });
    }

    fn load(path: &path::Path) -> Result<Config, String> {
        let read_result = fs::read_to_string(Config::config_file_path(path));
        match read_result {
            Ok(str) => {
                let jsonobj = json::parse(&str);
                match jsonobj {
                    Ok(obj) => {

                        return Ok(Config {
                            path: String::from(path.to_str().unwrap()),
                            db_file_name: String::from("db.sqlite3"),
                            current_branch: String::from(obj["current_branch"].as_str().unwrap()),
                            current_checked_out_commit: if obj["current_checked_out_commit"].is_null() {None} else {Some(obj["current_checked_out_commit"].as_str().unwrap().to_string())},
                            ignore_list: Config::load_ignore_list(path)?,
                        })
                    }
                    Err(jsonerr) => return Err(jsonerr.to_string()),
                }
            }
            Err(ioerr) => return Err(ioerr.to_string()),
        }
    }

    fn config_file_path(path: &path::Path) -> path::PathBuf {
        let mut config_path = path::PathBuf::from(path);
        config_path.push(".gudr/config.json");
        return config_path;
    }

    fn load_ignore_list(path: &path::Path) -> Result<HashSet<String>, String> {
        let mut ignorefile_path = path::PathBuf::from(path);
        ignorefile_path.push(consts::IGNORE_FILE);
        match fs::read_to_string(ignorefile_path) {
            Ok(str) => {
                return Ok(str.split_terminator('\n').map(|s| if &s[0..1] != "^" {format!("^{}", s)} else {s.to_owned()}).collect());
            }
            Err(ioerr) => {
                if ioerr.kind() == std::io::ErrorKind::NotFound {
                    info!("No ignore data");
                    return Ok(HashSet::new());
                }
                return Err(ioerr.to_string());
            }
        }
    }
}
