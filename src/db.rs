use std::fs;
use std::path;

use chrono::prelude::*;
use log::{trace, warn};
use serde::{Serialize, Deserialize};
use zstd;

use crate::config;
use crate::consts;
use crate::direntry::{DirEntry, DirEntryInfo, FileContents};
use crate::hash;

use rusqlite;

const MINIMUM_SIZE_FOR_COMPRESSION: usize = 1024;


enum FileStorageFormat {
    Raw = 1,
}

#[derive(PartialEq, Eq, Clone, Copy)]
enum FileCompressionFormat {
    NoCompression = 1,
    ZStd = 2,
}

enum FileEncryptionFormat {
    NoEncryption = 1,
}

#[derive(Serialize, Deserialize)]
struct CompressionParamsZStd {
    decompressed_size: usize,
}

mod sqlite_utils {
    pub fn execute(conn: &mut rusqlite::Connection, sql: &str) -> Result<(), String> {
        op(sql, conn.execute_batch(sql))?;
        return Ok(())
    }

    pub fn op<T>(err_ctx: &str, res: Result<T, rusqlite::Error>) -> Result<T, String> {
        match res {
            Err(e) => {
                return Err(format!("{}: {}", err_ctx, e));
            }
            Ok(x) => {
                return Ok(x);
            }
        }
    }

    pub fn drop_columns_supported() -> bool {
        // DROP COLUMN is supported since version 3.35.0, and a bug that could lead to DB corruption was fixed in 3.35.5
        // See https://sqlite.org/draft/changes.html
        return rusqlite::version_number() >= 3035005;
    }

    pub fn generalized_upsert_supported() -> bool {
        // Until version 3.35.0, sqlite required explictly identifying the columns with potential conflicts in an upsert (INSERT... ON CONFLICT... DO UPDATE) statement
        // See https://sqlite.org/draft/changes.html
        return rusqlite::version_number() >= 3035000;

    }
}

pub struct DB {
    conn: std::rc::Rc<rusqlite::Connection>,
}

#[derive(Clone)]
struct DbFileContents {
    db_conn: std::rc::Rc<rusqlite::Connection>,
    hash: hash::OwnedEncodedHash,
}

impl FileContents for DbFileContents {
    fn get(&self)-> std::rc::Rc<dyn AsRef<[u8]>> {
        std::rc::Rc::new(DB::file_contents(&self.db_conn, &self.hash))
    }

    fn create_copy(&self) -> Box<dyn FileContents> {
        Box::new(self.clone())
    }
}

impl DB{

    pub fn new(cfg: &config::Config) -> Result<DB, String> {
        let mut dbpath = path::PathBuf::from(&cfg.path);
        dbpath.push(format!(".gudr/{}", &cfg.db_file_name));
        let mut conn = match rusqlite::Connection::open(dbpath) {
            Ok(c) => c,
            Err(error) => {
                return Err(format!("{}: {}", cfg.db_file_name, error ));
            },
        };
        DB::initialize_db(&mut conn)?;
        return Ok(DB{conn: std::rc::Rc::new(conn)});
    }

    pub fn info(&self) -> String {
        return format!("DB Size: {}\n", self.size())
    }

    pub fn dump(&self) -> String {
        let mut result = String::new();


        result.push_str("Commits:\n");
        let sql = "SELECT hash FROM commits;";
        let mut stmt = self.conn.prepare(sql).unwrap();
        let mut commit_rows = stmt.query([]).unwrap();

        while let Ok(Some(row)) = commit_rows.next() {
            result.push_str(&format!("\t{}\n", &row.get_unwrap::<usize, String>(0))[..]);
        }

        result.push_str("Trees:\n");
        let sql = "SELECT files.hash FROM files JOIN file_parents ON (files.hash = file_parents.file_hash) WHERE file_parents.parent_hash='';";
        let mut stmt = self.conn.prepare(sql).unwrap();
        let mut commit_rows = stmt.query([]).unwrap();

        while let Ok(Some(row)) = commit_rows.next() {
            result.push_str(&format!("\t{}\n", &row.get_unwrap::<usize, String>(0))[..]);
        }


        result.push_str("Files:\n");
        let sql = "SELECT hash, path FROM files;";
        let mut stmt = self.conn.prepare(sql).unwrap();
        let mut commit_rows = stmt.query([]).unwrap();

        while let Ok(Some(row)) = commit_rows.next() {
            result.push_str(&format!("\t{} | {}\n", &row.get_unwrap::<usize, String>(0)[..], &row.get_unwrap::<usize, String>(1)[..]));
        }

        return result;
    }

    pub fn file_insert(&mut self, hash: hash::EncodedHash, properties: &DirEntryInfo, contents: &[u8],parent_hash: Option<hash::EncodedHash>, tree_hash: hash::EncodedHash) -> Result<hash::OwnedEncodedHash, String> {
        if hash.is_empty() {
            warn!("Hash for '{}' is empty!", properties.path.to_string_lossy());
        }
        trace!("Inserting file '{}' with hash '{}' and parent {:?} into tree {:?}", properties.path.to_string_lossy(),
                                                                                    &hash[..6], 
                                                                                    &parent_hash.and_then(|x|Some(x)).unwrap_or(&"~NONE~")[..6],
                                                                                    &tree_hash[..6], 
                                                                                );
        assert_ne!(hash, parent_hash.unwrap_or(&""));
        // Note: the following may be problematic: Rc::get_mut will return None if there are any other refs to the same object, which if there are any existing
        // DbFileContents around there will be. May need to use Rc<RefCell<>> if we hit this issue.
        let transaction =  sqlite_utils::op("", std::rc::Rc::get_mut(&mut self.conn).ok_or("Mutating DB with active R/O references")?.transaction())?;
        {
            let sql_file = "INSERT OR IGNORE INTO files (hash, path, properties) VALUES (:hash, :path, :properties);";
            let mut stmt_file = sqlite_utils::op(sql_file, transaction.prepare(sql_file)).unwrap();

            let file_params = rusqlite::named_params!{
                ":hash": &hash, 
                ":path": properties.path.to_string_lossy(),
                ":properties": bincode::serialize(&properties).unwrap(),
            };

            sqlite_utils::op(sql_file, stmt_file.execute(file_params))?;
        }

        {
            //it is possible for all files in a directory to not change, so the file's hash and the parent's hash will be the same.
            let sql_parent = "INSERT OR IGNORE INTO file_parents (file_hash, parent_hash) VALUES(:file_hash, :parent_hash);";
            let mut stmt_parent = sqlite_utils::op(sql_parent, transaction.prepare(sql_parent)).unwrap();

            let parent_hash_not_none = parent_hash.unwrap_or(&"");
            let parent_params = rusqlite::named_params!{
                ":file_hash": hash,
                ":parent_hash": parent_hash_not_none,
            };
            sqlite_utils::op(sql_parent, stmt_parent.execute(parent_params)).unwrap();
        }

        {
            let sql_tree = "INSERT OR IGNORE INTO file_trees (file_hash, tree_hash) VALUES(:file_hash, :tree_hash);";
            let mut stmt_tree = sqlite_utils::op(sql_tree, transaction.prepare(sql_tree)).unwrap();

            let tree_params = rusqlite::named_params!{
                ":file_hash": hash,
                ":tree_hash": tree_hash,
            };
            sqlite_utils::op(sql_tree, stmt_tree.execute(tree_params)).unwrap();
        }

        {
            let mut compression = FileCompressionFormat::NoCompression;
            let mut compressed_contents = vec![];
            let mut compression_params = vec![];
            if contents.len() > MINIMUM_SIZE_FOR_COMPRESSION {
                let compress_result = zstd::bulk::compress(contents, 0);
                if let Err(err) = compress_result{
                    warn!("Failed to compress data for file {} because {}", properties.path.to_string_lossy(), err);
                } else {
                    compressed_contents = compress_result.unwrap();
                    compression = FileCompressionFormat::ZStd;
                    let params = CompressionParamsZStd{decompressed_size: contents.len()};
                    compression_params = bincode::serialize(&params).unwrap()
                }
            }

            let sql_tree = "INSERT OR IGNORE INTO file_contents (file_hash, format, compression, compression_params, encryption, contents) VALUES(:file_hash, :format, :compression, :compression_params, :encryption, :contents);";
            let mut stmt_tree = sqlite_utils::op(sql_tree, transaction.prepare(sql_tree)).unwrap();

            let tree_params = rusqlite::named_params!{
                ":file_hash": hash,
                ":format": FileStorageFormat::Raw as i64, 
                ":compression": compression as i64,
                ":compression_params": compression_params,
                ":encryption": FileEncryptionFormat::NoEncryption as i64,
                ":contents": if compression == FileCompressionFormat::NoCompression {contents} else {&compressed_contents}, 
            };
            sqlite_utils::op(sql_tree, stmt_tree.execute(tree_params)).unwrap();
        }

        sqlite_utils::op("", transaction.commit())?;

        return Ok(hash.to_owned());
    }

    pub fn file_get(&self, hash: hash::EncodedHash) -> Result<DirEntry, String> {
        let sql = "SELECT * FROM files WHERE hash=:hash";
        let mut stmt = sqlite_utils::op(sql, self.conn.prepare(sql))?;

        let params = rusqlite::named_params!{
            ":hash": hash,
        };

        let mut rows = sqlite_utils::op(sql, stmt.query(params))?;

        return match sqlite_utils::op(sql, rows.next())? {
            Some(row) => {
                Ok(self.dir_entry_from_row(&row))
            },
            None => {Err(format!("file {} not found", hash))}
        }
    }

    pub fn file_get_by_tree_and_path(&self, tree_hash: hash::EncodedHash, path: &str) -> Result<DirEntry, String> {
        let sql = "SELECT * FROM files JOIN file_trees ON (files.hash=file_trees.file_hash) WHERE file_trees.tree_hash=:tree_hash AND files.path = :path";
        let mut stmt = sqlite_utils::op(sql, self.conn.prepare(sql))?;
        let params = rusqlite::named_params!{
            ":tree_hash": tree_hash,
            ":path": &path,
        };

        let mut rows = sqlite_utils::op(sql, stmt.query(params))?;

        return match sqlite_utils::op(sql, rows.next())? {
            Some(row) => {
                Ok(self.dir_entry_from_row(&row))
            },
            None => {Err(format!("path '{}' not found in tree {}", path, tree_hash))}
        }
    }

    pub fn file_get_children(&self, hash: hash::EncodedHash) -> Result<Vec<DirEntry>, String> {
        let sql = "SELECT * FROM files JOIN file_parents ON (files.hash=file_parents.file_hash) WHERE file_parents.parent_hash=:hash ORDER BY path";
        let mut stmt = sqlite_utils::op(sql, self.conn.prepare(sql))?;

        let params = rusqlite::named_params!{
            ":hash": hash,
        };

        let rows = sqlite_utils::op(sql, stmt.query(params))?;

        Ok(rows.mapped(|row| Ok(self.dir_entry_from_row(row))).map(Result::unwrap).collect())
    }

    pub fn file_get_children_by_raw_hash(&self, hash: hash::RawHash) -> Result<Vec<DirEntry>, String> {
        let sql = "SELECT * FROM files JOIN file_parents ON (files.hash=file_parents.file_hash) WHERE file_parents.parent_hash=:hash ORDER BY path";
        let mut stmt = sqlite_utils::op(sql, self.conn.prepare(sql))?;

        let params = rusqlite::named_params!{
            ":hash": hash::encode(&hash),
        };

        let rows = sqlite_utils::op(sql, stmt.query(params))?;

        Ok(rows.mapped(|row| Ok(self.dir_entry_from_row(row))).map(Result::unwrap).collect())
    }

    pub fn commit_insert(
        &mut self, 
        commit_hash: hash::EncodedHash, 
        description: &str, 
        user: &str, 
        encoded_tree_hash: hash::EncodedHash, 
        parent_hash: hash::EncodedHash,
        merged_parent_hash: Option<hash::EncodedHash>,
    ) -> Result<hash::OwnedEncodedHash, String>  {
        let sql = "INSERT INTO commits (hash, desc, timestamp, user, tree, parent_hash, merged_parent_hash) VALUES (:hash, :desc, :timestamp, :user, :tree, :parent_hash, :merged_parent_hash);";
        let mut stmt = sqlite_utils::op(sql, self.conn.prepare(sql))?;

        let params = rusqlite::named_params!{
            ":hash": commit_hash,
            ":desc": description,
            ":timestamp": chrono::offset::Utc::now().to_rfc3339(),
            ":user": user,
            ":tree": encoded_tree_hash,
            ":parent_hash": parent_hash,
            ":merged_parent_hash": merged_parent_hash
        };

        let updated_rows = sqlite_utils::op(sql, stmt.execute(params))?;
        if updated_rows != 1 {
            return Err("Commit was not inserted".to_owned());
        }

        return Ok(commit_hash.to_owned());
    }

    pub fn commit_get(&self, commit_hash: hash::EncodedHash) -> Result<consts::Commit, String> {
        let mut get_result = self.commit_get_multi(&mut [commit_hash.to_owned()].iter());
        if get_result.is_empty() {
            return Err(format!("commit {} not found", commit_hash));
        }

        Ok(get_result.remove(0))
    }

    pub fn commit_get_multi<'a>(&self, commits: impl Iterator<Item = &'a String>) -> Vec<consts::Commit>
    {
        let sql = "SELECT * FROM commits WHERE hash IN (:commit_hashes)";
        let mut stmt = sqlite_utils::op(sql, self.conn.prepare(sql)).unwrap();

        let commit_hashes_as_str: String = itertools::intersperse(commits.cloned(), String::from(",")).collect();
        let params = rusqlite::named_params!{
            ":commit_hashes": commit_hashes_as_str,
        };

        let transformed_rows = sqlite_utils::op(sql, stmt.query_map(params, |r| Ok(DB::commit_from_row(r)))).unwrap();

        return transformed_rows.map(|r| r.unwrap()).collect();
    }

    pub fn commit_get_by_partial_hash(&self, partial_hash: hash::EncodedHash) -> Result<Vec<consts::Commit>, String> {
        let sql = "SELECT * FROM commits WHERE hash LIKE :hash";
        let mut stmt = sqlite_utils::op(sql, self.conn.prepare(sql))?;

        let params = rusqlite::named_params!{
            ":hash": format!("{}%", partial_hash),
        };

        let transformed_rows = sqlite_utils::op(sql, stmt.query_map(params, |r| {Ok(DB::commit_from_row(r))}))?;

        return Ok(transformed_rows.map(|r| r.unwrap()).collect());
    }

    pub fn branch_get(&self, branch: &str) -> Result<consts::RefInfo, String>{
         self.ref_get(branch, consts::RefType::Branch)
    }

    pub fn branch_set_head(&mut self, branch: &str, commit_hash: hash::EncodedHash) -> Result<(), String>{
        let sql = "UPDATE refs SET head = :head WHERE type = :type AND name = :name;";
        let mut stmt = sqlite_utils::op(sql, self.conn.prepare(sql))?;

        let params = rusqlite::named_params!{
            ":head": commit_hash,
            ":type": consts::RefType::Branch as i64,
            ":name": branch,
        };

        sqlite_utils::op(sql, stmt.execute(params))?;

        Ok(())
    }

    pub fn branch_create(&mut self, branch: &str, commit_hash: hash::EncodedHash) -> Result<(), String>{
        let sql = "INSERT INTO refs (head, type, name) VALUES(:head, :type, :name);";
        let mut stmt = sqlite_utils::op(sql, self.conn.prepare(sql))?;

        let params = rusqlite::named_params!{
            ":head": commit_hash,
            ":type": consts::RefType::Branch as i64,
            ":name": branch,
        };

        sqlite_utils::op(sql, stmt.execute(params))?;

        Ok(())
    }

    pub fn branch_update_remote(&mut self, origin: &str, branch: &str, commit_hash: hash::EncodedHash) -> Result<consts::ResolvedRef, String> {
        Ok(consts::ResolvedRef::Branch(self.ref_upsert(origin, consts::RefType::Branch, branch, commit_hash)?))
    }

    pub fn tag_get(&self, tag: &str) -> Result<consts::RefInfo, String> {
        return Ok(self.ref_get(tag, consts::RefType::Tag)?);
    }

    pub fn tag_set(&mut self, tag: &str, commit_hash: hash::EncodedHash) -> Result<(), String> {
        let sql = "INSERT INTO refs (head, type, name) VALUES(:head, :type, :name);";
        let mut stmt = sqlite_utils::op(sql, self.conn.prepare(sql))?;

        let params = rusqlite::named_params!{
            ":head": commit_hash,
            ":type": consts::RefType::Tag as i64,
            ":name": tag,
        };

        sqlite_utils::op(sql, stmt.execute(params))?;

        Ok(())
    }

    pub fn tag_set_remote(&mut self, origin: &str, tag: &str, commit_hash: hash::EncodedHash) -> Result<consts::ResolvedRef, String> {
        Ok(consts::ResolvedRef::Tag(self.ref_upsert(origin, consts::RefType::Tag, tag, commit_hash)?))
    }

    pub fn ref_list(&self) -> Result<Vec<consts::ResolvedRef>, String> {
        let sql = "SELECT * FROM refs";
        let mut stmt = sqlite_utils::op(sql, self.conn.prepare(sql))?;

        let transformed_rows = sqlite_utils::op(sql, stmt.query_map([], |row|{Ok(DB::resolved_ref_from_row(row))}))?;

        return Ok(transformed_rows.map(|r| r.unwrap()).collect());
    }

    pub fn ref_list_from_origin(&self, origin_name: &str) -> Result<Vec<consts::ResolvedRef>, String> {
        let sql = "SELECT * FROM refs WHERE origin=:origin";
        let mut stmt = sqlite_utils::op(sql, self.conn.prepare(sql))?;

        let params = rusqlite::named_params!{
            ":origin": origin_name,
        };

        let transformed_rows = sqlite_utils::op(sql, stmt.query_map(params, |row|{Ok(DB::resolved_ref_from_row(row))}))?;

        return Ok(transformed_rows.map(|r| r.unwrap()).collect());
    }

    fn ref_get(&self, ref_name: &str, ref_type: consts::RefType) -> Result<consts::RefInfo, String> {
        let sql = "SELECT * FROM refs WHERE type=:type AND name=:name";
        let mut stmt = sqlite_utils::op(sql, self.conn.prepare(sql))?;

        let params = rusqlite::named_params!{
            ":type": ref_type as i64,
            ":name": ref_name,
        };

        let mut rows = sqlite_utils::op(sql, stmt.query(params))?;
        return match sqlite_utils::op(sql, rows.next())? {
            Some(row) => {
                Ok(DB::ref_from_row(row))
            },
            None => {Err(format!("ref `{}' not found", ref_name))}
        }
    }

    fn ref_upsert(&mut self, origin: &str, ref_type: consts::RefType, name: &str, commit_hash: hash::EncodedHash) -> Result<consts::RefInfo, String> {

        let sql_conflict_columns = if sqlite_utils::generalized_upsert_supported() {""} else {"(origin, name)"};
        let sql = format!("INSERT INTO refs (origin, head, type, name) VALUES(:origin, :head, :type, :name) ON CONFLICT {} DO UPDATE SET head = :head WHERE origin=:origin AND name=:name;", sql_conflict_columns);
        let mut stmt = sqlite_utils::op(&sql, self.conn.prepare(&sql))?;

        let params = rusqlite::named_params!{
            ":origin": origin,
            ":head": commit_hash,
            ":type": ref_type as i64,
            ":name": name,
        };

        sqlite_utils::op(&sql, stmt.execute(params))?;

        Ok(consts::RefInfo{commit: Some(commit_hash.to_string()), name: name.to_string(), origin: Some(origin.to_string())}) 
    }

    fn size(&self) -> u64 {
        return fs::metadata(&self.conn.path().unwrap()).unwrap().len();
    }

    fn initialize_db(conn: &mut rusqlite::Connection) -> Result<(), String>{
        migrations::run_migrations(conn)
    }

    fn parse_utc_datetime(timestamp: &str) -> chrono::DateTime<chrono::offset::Utc>{
        let fixed_offset = chrono::DateTime::parse_from_rfc3339(&timestamp).unwrap();
        chrono::DateTime::<Utc>::from(fixed_offset)
    }

    fn dir_entry_from_row(&self, row: &rusqlite::Row) -> DirEntry{
        let serialized_properties = row.get::<&str, Vec<u8>>("properties").unwrap();
        let file_hash = row.get::<&str, String>("hash").unwrap();
        DirEntry::new(bincode::deserialize(&serialized_properties[..]).unwrap(), 
                                  Box::new(DbFileContents{db_conn: self.conn.clone(), hash: file_hash.clone()}),
                                  Box::new(hash::ConstDirEntryHash{hash_value: hash::decode(&file_hash).unwrap()}))
    }

    fn resolved_ref_from_row(row: &rusqlite::Row) -> consts::ResolvedRef {
        let type_val:i64 = row.get("type").unwrap();
        if type_val == consts::RefType::Branch as i64 {
            consts::ResolvedRef::Branch(DB::ref_from_row(row))
        } else if type_val == consts::RefType::Tag as i64 {
            consts::ResolvedRef::Tag(DB::ref_from_row(row))
        } else {
            panic!("Unrecognized ref type {}", type_val);
        }
}

    fn ref_from_row(row: &rusqlite::Row) -> consts::RefInfo {
            consts::RefInfo{name: row.get("name").unwrap(), 
                            origin: row.get("origin").unwrap(), 
                            commit: row.get("head").unwrap()}
    }

    fn file_contents(conn: &rusqlite::Connection, hash: hash::EncodedHash) -> Vec<u8>{
        let sql = "SELECT * FROM file_contents WHERE file_hash=:hash";
        let mut stmt = sqlite_utils::op(sql, conn.prepare(sql)).unwrap();

        let params = rusqlite::named_params!{
            ":hash": hash,
        };

        let mut rows = sqlite_utils::op(sql, stmt.query(params)).unwrap();
        let row = sqlite_utils::op(sql, rows.next()).unwrap().unwrap();
        let compression = row.get::<&str, i64>("compression").unwrap();
        if compression == FileCompressionFormat::NoCompression as i64{
            row.get::<&str, Vec<u8>>("contents").unwrap()
        } else {
            let zstd_params: CompressionParamsZStd = bincode::deserialize(&row.get::<&str, Vec<u8>>("compression_params").unwrap()).unwrap();
            zstd::bulk::decompress(&row.get::<&str, Vec<u8>>("contents").unwrap(), zstd_params.decompressed_size).unwrap()
        }
    }

    fn commit_from_row(row: &rusqlite::Row) -> consts::Commit{
        consts::Commit{
            hash: row.get::<&str, String>("hash").unwrap(), 
            description: row.get("desc").unwrap(), 
            timestamp: DB::parse_utc_datetime(&row.get::<&str, String>("timestamp").unwrap()),
            user: row.get("user").unwrap(),
            tree_hash: row.get_unwrap::<&str, String>("tree"),
            parent_hash: row.get::<&str, String>("parent_hash").unwrap(),
            merged_parent_hash: row.get_unwrap("merged_parent_hash"),
        }
    }
}

mod migrations {

    use log::{debug, info};

    use crate::consts;
    use crate::db::sqlite_utils;
    use crate::hash;

    type MigrationFunc = fn(&mut rusqlite::Connection) -> Result<(), String>;

    fn calculate_hash(t: &str) -> String {
        hash::encode(t.as_bytes())
    }

    macro_rules! migration{
        ($m:expr) => {
            (calculate_hash(stringify!($m)), $m)
        };
    }

    pub fn run_migrations(conn: &mut rusqlite::Connection) -> Result<(), String> {
        sqlite_utils::execute(conn, 
            "
            CREATE TABLE IF NOT EXISTS last_migration (version TEXT PRIMARY KEY);
            ")?;
        let last_migration = get_last_migration(conn).unwrap();
        info!("Current DB version: {:?}", &last_migration);
        let mut should_run_migrations = if let None = last_migration {true} else {false};
        for (migration_version, migration) in get_migrations() {
            debug!("Migration: {}...", migration_version);
            if should_run_migrations {
                migration(conn)?;
                sqlite_utils::execute(conn, "DELETE FROM last_migration;")?;
                sqlite_utils::execute(conn, &format!("INSERT INTO last_migration (version) VALUES ('{}');", migration_version))?;
                debug!("Done.");
            } else {
                debug!("Skipping.");
                should_run_migrations = match last_migration { None => true, Some(ref current_version) => current_version == &migration_version};
            }
        }

        Ok(())
    }

    fn get_migrations() -> Vec<(String, MigrationFunc)> {
        vec![
            migration!(initial_db),
            migration!(commit_has_two_parents),
            migration!(separate_file_contents),
            migration!(fill_in_encryption_compression),
        ]
    }

    fn get_last_migration(conn: &rusqlite::Connection) -> Result<Option<String>, String> {
        let sql = "SELECT version FROM last_migration";
        let mut stmt = sqlite_utils::op(sql, conn.prepare(sql))?;

        let mut rows = sqlite_utils::op(sql, stmt.query([]))?;

        return match sqlite_utils::op(sql, rows.next())? {
            Some(row) => {Ok(Some(row.get(0).unwrap()))},
            None => {Ok(None)}
        }
    }

    fn initial_db(conn: &mut rusqlite::Connection) -> Result<(), String> {
        sqlite_utils::execute(conn, 
            "
            CREATE TABLE IF NOT EXISTS files (hash TEXT PRIMARY KEY, path TEXT, format INTEGER, properties BLOB, contents BLOB);
            ")?;
        sqlite_utils::execute(conn, 
            "
            CREATE TABLE IF NOT EXISTS file_parents (file_hash TEXT REFERENCES files(hash), parent_hash TEXT REFERENCES files(hash), UNIQUE(file_hash, parent_hash));
            ")?;
        sqlite_utils::execute(conn, 
            "
            CREATE TABLE IF NOT EXISTS file_trees (file_hash TEXT REFERENCES files(hash), tree_hash TEXT REFERENCES files(hash), UNIQUE(file_hash, tree_hash));
            ")?;
        sqlite_utils::execute(conn,
            "
            CREATE TABLE IF NOT EXISTS commits (hash TEXT PRIMARY KEY, desc TEXT, timestamp TEXT, user TEXT, tree TEXT REFERENCES files(hash), parent_hash TEXT REFERENCES commits(hash));
            ")?;
        sqlite_utils::execute(conn, 
            "
            CREATE TABLE IF NOT EXISTS  refs (name TEXT, origin TEXT, type INTEGER, head TEXT REFERENCES commits(hash), PRIMARY KEY (name, origin));
            ")?;
        sqlite_utils::execute(conn, 
            &format!("
            INSERT INTO refs (name, type, head) VALUES ('{}', {}, NULL)
            ", consts::DEFAULT_BRANCH, consts::RefType::Branch as u32))?;

        Ok(())
    }

    fn commit_has_two_parents(conn: &mut rusqlite::Connection) -> Result<(), String> {
        sqlite_utils::execute(conn, "ALTER TABLE commits ADD COLUMN merged_parent_hash TEXT REFERENCES commits(hash)")
    }

    fn separate_file_contents(conn: &mut rusqlite::Connection) -> Result<(), String> {
        sqlite_utils::execute(conn, 
            "
            CREATE TABLE IF NOT EXISTS file_contents (file_hash TEXT REFERENCES files(hash) PRIMARY KEY, format INTEGER, format_params BLOB, compression INTEGER, compression_params BLOB, encryption INTEGER, encryption_params BLOB, contents BLOB);
            ")?;

        sqlite_utils::execute(conn, 
            "
            INSERT INTO file_contents (file_hash, format, contents) SELECT hash, format, contents FROM files;
            ")?;

        if sqlite_utils::drop_columns_supported() {
            //otherwise the columns will be there but we won't use them.
            sqlite_utils::execute(conn, 
                "ALTER TABLE files DROP COLUMN format;"
                )?;

            sqlite_utils::execute(conn, 
                "ALTER TABLE files DROP COLUMN contents;"
                )?;
        }
        Ok(())
    }

    fn fill_in_encryption_compression(conn: &mut rusqlite::Connection) -> Result<(), String> {
        sqlite_utils::execute(conn, "UPDATE file_contents SET encryption = 1 WHERE encryption ISNULL;")?;
        sqlite_utils::execute(conn, "UPDATE file_contents SET compression = 1 WHERE compression ISNULL;")?;

        Ok(())
    }

}