use std::path;
use std::fs;

#[cfg(test)]
pub fn expect_error(result: Result<String, String>) -> String {
    match result {
        Ok(s) => {format!("Expected error, got Ok('{}')", s)}
        Err(errstr) => {errstr}
    }
}

#[cfg(test)]
pub fn get_test_dir(test_file: &str, test_name: &str) -> String{
    let path = format!("./test_data/{}/{}", path::Path::new(test_file).file_stem().unwrap().to_string_lossy(), test_name);
    if std::path::Path::new(&path).exists(){
        fs::remove_dir_all(&path).unwrap();
    }
    return path;
}

#[cfg(test)]
pub fn prepare_test_dir(test_file: &str, test_name: &str) -> String{
    let path = format!("./test_data/{}/{}", path::Path::new(test_file).file_stem().unwrap().to_string_lossy(), test_name);
    if std::path::Path::new(&path).exists(){
        fs::remove_dir_all(&path).unwrap();
    }
    return path;
}