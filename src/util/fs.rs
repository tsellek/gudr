use std::path;
use std::fs;

use memmap::MmapOptions;

use crate::direntry::{DirEntryInfo, FileContents};
use crate::hash;

pub fn op<T>(fs_result: Result<T, std::io::Error>) -> Result<T, String> {
    if let Err(ioerr) = fs_result {
        return Err(ioerr.to_string());
    } else {
        return Ok(fs_result.unwrap());
    }
}


pub fn hash_file(finfo: &DirEntryInfo, contents: &dyn FileContents) -> hash::OwnedRawHash{
    hash_file_data(finfo.unix_permissions, finfo.path.to_str().unwrap(), (*contents.get()).as_ref())
}

pub fn hash_file_data(unix_permissions: u32, path: &str, contents: &[u8]) -> hash::OwnedRawHash{
    hash::hash_bytes(&[&unix_permissions.to_be_bytes()[..], 
                     &path.as_bytes()[..], 
                     &contents[..]])
                    .to_owned()
}

pub fn file_contents(path: &path::Path) -> Result<std::rc::Rc<dyn AsRef<[u8]>>, String>{
    if path.metadata().unwrap().len() == 0
    {
        return Ok(std::rc::Rc::new(vec![]));
    }
    let mmap = op(unsafe {
        MmapOptions::new().map(&op(fs::File::open(path))?)
    })?;
    let contents = std::rc::Rc::new(mmap);
    return Ok(contents);
}
