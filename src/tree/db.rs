use crate::direntry::DirEntry;
use crate::db;
use crate::hash;
use crate::tree;

pub struct DbTree<'a> {
    db: &'a db::DB,
    hash: hash::OwnedEncodedHash,
}

impl<'a> DbTree<'a> {
    pub fn new(db: &'a db::DB, hash: hash::EncodedHash) -> DbTree<'a> {
        return DbTree{ db, hash: hash.to_owned() };
    }
}

impl<'a> tree::Tree for DbTree<'a> {

    fn root(&self) -> DirEntry {
        self.db.file_get(&self.hash).unwrap()
    }

    fn get_children(&self, entry: &DirEntry) -> Result<Vec<DirEntry>, String> {
        self.db.file_get_children_by_raw_hash(&entry.get_hash())
    }
}

#[cfg(test)]
mod run_tests {
    use std::fs;
    use std::path;
    
    use crate::db::DB;
    use crate::direntry::{DirEntry, DirEntryInfo, DirEntryType};
    use crate::hash;
    use crate::tree;

    use insta;

    #[test]
    fn test_db_tree_walk() {
        let path = "./test_data/tree/test_db_tree_walk";
        let mut db = setup_repo_and_db(&path);
        create_file_tree_in_db(&mut db, &path);

        let mut result: Vec<DirEntry> = Vec::new();
        let db_tree = tree::db::DbTree::new(&db, &fake_hash(&path));

        tree::walk(&db_tree, &mut |params| result.push(params.direntry.clone())).unwrap();

        insta::assert_debug_snapshot!(result);
    }

    fn setup_repo_and_db(path: &str) -> DB {

        if path::Path::new(path).exists() {
            fs::remove_dir_all(path).unwrap();
        }

        let mut dbpath = path::PathBuf::from(path);
        dbpath.push(".gudr");
        fs::create_dir_all(&dbpath).unwrap();

        let mut cfg = crate::config::Config::new(path::Path::new(path)).unwrap();
        cfg.db_file_name = "db.sqlite3".to_owned();
        DB::new(&cfg).unwrap()
    }

    fn create_file_tree_in_db(db: &mut DB, path: &str) {
        let tree_hash = fake_hash(path);
        let props = DirEntryInfo {
            detype: DirEntryType::Dir,
            path: path::PathBuf::from(path),
            unix_permissions: 0,
        };
        db.file_insert(&fake_hash(&path), &props, &[], None, &tree_hash).unwrap();

        for subdir in ["empty_dir", "non_empty_dir"] {
            let mut subdir_path = path::PathBuf::from(path);
            subdir_path.push(subdir);
            let props = DirEntryInfo {
                detype: DirEntryType::Dir,
                path: subdir_path,
                unix_permissions: 0,
            };
            db.file_insert(
                &fake_hash(&subdir),
                &props,
                &[],
                Some(&fake_hash(&path)),
                &tree_hash,
            )
            .unwrap();
        }

        let parent_hash = fake_hash("non_empty_dir");
        for filename in ["file1.txt", "file2.txt"] {
            let mut file_path = path::PathBuf::from(path);
            file_path.push(format!("non_empty_dir/{}", filename));
            let props = DirEntryInfo {
                detype: DirEntryType::File,
                path: file_path,
                unix_permissions: 0,
            };
            db.file_insert(
                &fake_hash(&props.path.to_string_lossy()),
                &props,
                &props.path.to_string_lossy().as_bytes(),
                Some(&parent_hash), 
                &tree_hash,
            )
            .unwrap();
        }

        let mut file_path = path::PathBuf::from(path);
        file_path.push("top_level.txt");
        let props = DirEntryInfo {
            detype: DirEntryType::File,
            path: file_path,
            unix_permissions: 0,
        };
        db.file_insert(
            &fake_hash(&props.path.to_string_lossy()),
            &props,
            &[],
            Some(&fake_hash(&path)), 
            &tree_hash,
        )
        .unwrap();
    }

    fn fake_hash(string: &str) -> hash::OwnedEncodedHash {
        hash::encode(string.as_bytes())
    }
}