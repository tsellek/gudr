use std::fs;
use std::path;

use crate::direntry::{DirEntry, DirEntryType};
use crate::filesystem::Filesystem;
use crate::tree;
use crate::util;

pub struct FsTree<'a> {
    path: &'a path::Path,
    fs: &'a Filesystem,
}


impl<'a> FsTree<'a> {
    pub fn new(fs: &'a Filesystem, path: &'a path::Path) -> FsTree<'a> {
        return FsTree { fs, path };
    }
}

impl<'a> tree::Tree for FsTree<'a> {
    fn root(&self) -> DirEntry {
        self.fs.read_direntry(&self.path, &self.path).unwrap()
    }

    fn get_children(&self, entry: &DirEntry) -> Result<Vec<DirEntry>, String> {
        let actual_path: path::PathBuf = [self.path, &entry.info.path].iter().collect();
        
        if entry.info.detype != DirEntryType::Dir {
            return Ok(vec![]);
        }

        let mut result = vec![];
        for entry in util::fs::op(fs::read_dir(&actual_path))? {
            let entry = util::fs::op(entry)?;
            result.push(self.fs.read_direntry(&self.path, &entry.path())?);
        }

        result.sort_unstable_by(|x, y| {x.info.path.partial_cmp(&y.info.path).unwrap()} );

        return Ok(result);
    }
}

#[cfg(test)]
mod run_tests {
    use std::fs;
    use std::io::Write;
    use std::path;

    use crate::direntry::DirEntry;
    use crate::filesystem::Filesystem;
    use crate::tree;
    use crate::util::test;

    use ::function_name::named;
    use insta;

    #[test]
    #[named]
    fn test_fs_tree_walk() {
        let path = test::prepare_test_dir(file!(), function_name!());
        fs::create_dir_all(&path).unwrap();
        for subdir in ["empty", "non_empty"] {
            let mut subdir_path = path::PathBuf::from(&path);
            subdir_path.push(subdir);
            fs::create_dir(subdir_path).unwrap();
        }
        for filename in ["file1.txt", "file2.txt"] {
            let mut file_path = path::PathBuf::from(&path);
            file_path.push(format!("non_empty/{}", filename));
            let mut file = fs::File::create(file_path).unwrap();
            write!(file, "{}", filename).unwrap();
        }
        let mut file_path = path::PathBuf::from(&path);
        file_path.push("top_level.txt");
        let mut file = fs::File::create(file_path).unwrap();
        write!(file, "{}", "top_level.txt").unwrap();

        let mut result: Vec<DirEntry> = Vec::new();
        let fs = Filesystem::new();
        let fs_tree = tree::fs::FsTree::new(&fs, path::Path::new(&path));

        tree::walk(&fs_tree, &mut |params| result.push(params.direntry.clone())).unwrap();

        insta::assert_debug_snapshot!(result);
    }

    #[test]
    #[named]
    fn test_fs_tree_walk_empty_repo() {
        let path = test::prepare_test_dir(file!(), function_name!());
        fs::create_dir_all(&path).unwrap();
        let mut file_path = path::PathBuf::from(&path);
        file_path.push(".gudr.ignore");
        let mut file = fs::File::create(file_path).unwrap();
        write!(file, "{}", crate::consts::DEFAULT_IGNORE_LIST.join("\n")).unwrap();

        let mut result: Vec<DirEntry> = Vec::new();
        let fs = Filesystem::new();
        let fs_tree = tree::fs::FsTree::new(&fs, path::Path::new(&path));

        tree::walk(&fs_tree, &mut |params| result.push(params.direntry.clone())).unwrap();

        insta::assert_debug_snapshot!(result);
    }

    #[test]
    #[named]
    fn test_fs_tree_walk_empty_file() {
        let path = test::prepare_test_dir(file!(), function_name!());
        if path::Path::new(&path).exists() {
            fs::remove_dir_all(&path).unwrap();
        }
        fs::create_dir_all(&path).unwrap();
        let mut file_path = path::PathBuf::from(&path);
        file_path.push("some_file.txt");
        fs::File::create(file_path).unwrap();

        let mut result: Vec<DirEntry> = Vec::new();
        let fs = Filesystem::new();
        let fs_tree = tree::fs::FsTree::new(&fs, path::Path::new(&path));

        tree::walk(&fs_tree, &mut |params| result.push(params.direntry.clone())).unwrap();

        insta::assert_debug_snapshot!(result);
    }
}
