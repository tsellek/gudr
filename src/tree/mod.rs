pub mod fs;
pub mod db;

use log::{trace};

use crate::direntry::{DirEntry};
use crate::hash::RawHash;

pub struct WalkCallbackParams<'a> {
    pub direntry: &'a DirEntry,
    pub parent_hash: Option<RawHash<'a>>,
}

impl<'a> std::fmt::Debug for WalkCallbackParams<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        f.debug_struct("WalkCallbackResults")
            .field("direntry", &self.direntry)
            .field("contents", &Vec::from((*(*self.direntry.contents).get()).as_ref()))
            .finish()
    }
}

pub type WalkCallback<'a> = dyn FnMut(WalkCallbackParams) + 'a;

pub enum DiffWalkCallbackParams<'a> {
    Added(&'a WalkCallbackParams<'a>),
    Deleted(&'a WalkCallbackParams<'a>),
    Diff(&'a WalkCallbackParams<'a>, &'a WalkCallbackParams<'a>),
}


pub type DiffWalkCallback<'a> = dyn FnMut(&DiffWalkCallbackParams) + 'a;


pub trait Tree {
    //fn walk(&self, include_contents: bool, cb: &mut WalkCallback) -> Result<(), String>;
    fn root(&self) -> DirEntry;
    fn get_children(&self, entry: &DirEntry) -> Result<Vec<DirEntry>, String>;
}

fn do_walk_diff(tree1: &dyn Tree, direntry1: Option<&DirEntry>, tree2: &dyn Tree, direntry2: Option<&DirEntry>, cb: &mut DiffWalkCallback) -> Result<(), String> {
    match (direntry1, direntry2) {
        (Some(entry1), Some(entry2)) => {
            trace!("'{}' <?> '{}'", entry1.info.path.to_string_lossy(), entry2.info.path.to_string_lossy());
            if entry1.get_hash() != entry2.get_hash() {
                trace!(" X [{}] vs. [{}]", &hex::encode(entry1.get_hash()), &hex::encode(entry2.get_hash()));
                cb(&DiffWalkCallbackParams::Diff(&walk_params_from_dir_entry(&entry1, None), &walk_params_from_dir_entry(&entry2, None)));
            }
            else {
                trace!(" =");
                return Ok(());
            }
            let mut child1_idx = 0;
            let mut child2_idx = 0;
            let children1 = tree1.get_children(&entry1)?;
            let children2 = tree2.get_children(&entry2)?;
            
            trace!("Children1: ");
            children1.iter().for_each(|c| trace!("{},", c.info.path.to_string_lossy()));
            trace!("\nChildren2: ");
            children2.iter().for_each(|c| trace!("{},", c.info.path.to_string_lossy()));
            while child1_idx < children1.len() && child2_idx < children2.len() {
                let child1 = &children1[child1_idx];
                let child2 = &children2[child2_idx];

                trace!(" >> '{}' <?> '{}'", child1.info.path.to_string_lossy(), child2.info.path.to_string_lossy());
                if child1.info.path == child2.info.path {
                    child1_idx += 1;
                    child2_idx += 1;
                    do_walk_diff(tree1, Some(&child1), tree2, Some(&child2), cb)?;
                } else if child1.info.path < child2.info.path {
                    child1_idx += 1;
                    do_walk_diff(tree1, Some(&child1), tree2, None, cb)?;
                } else {
                    child2_idx += 1;
                    do_walk_diff(tree1, None, tree2, Some(&child2), cb)?;
                }
            }
            let mut remainder = &children1[child1_idx..];
            let mut added = false;
            if remainder.len() == 0 {
                remainder = &children2[child2_idx..];
                added = true;
            }

            for child in remainder {
                if added {
                    do_walk_diff(tree1, None, tree2, Some(&child), cb)?;
                } else {
                    do_walk_diff(tree1, Some(&child), tree2, None, cb)?;
                }
            }
        }
        (None, Some(entry2)) => {
            trace!("{} +", entry2.info.path.to_string_lossy());
            cb(&DiffWalkCallbackParams::Added(&walk_params_from_dir_entry(&entry2, None)));
            for child in tree2.get_children(&entry2)? {
                do_walk_diff(tree1, None, tree2, Some(&child), cb)?;
            }
        }
        (Some(entry1), None) => {
            trace!("{} -", entry1.info.path.to_string_lossy());
            cb(&DiffWalkCallbackParams::Deleted(&walk_params_from_dir_entry(&entry1, None)));
            for child in tree1.get_children(&entry1)? {
                do_walk_diff(tree1, Some(&child), tree2, None, cb)?;
            }
        }
        (None, None) => {
            return Err("Diff walk should have at least one hash to handle".to_owned());
        }
    }
    Ok(())
}

fn do_walk(tree: &dyn Tree, entry: &DirEntry, cb: &mut WalkCallback) -> Result<(), String> {
    for ref child_entry in tree.get_children(entry)? {
        cb(WalkCallbackParams {
            direntry: &child_entry,
            parent_hash: Some(&entry.get_hash()),
        });
        do_walk(tree, child_entry, cb)?
    }
    Ok(())
}

fn walk_params_from_dir_entry<'a>(entry: &'a DirEntry, parent_hash: Option<RawHash<'a>>) -> WalkCallbackParams<'a> {
    WalkCallbackParams{
        direntry: entry,
        parent_hash: parent_hash,
    }
}

pub fn walk(tree: &dyn Tree, cb: &mut WalkCallback) -> Result<(), String> {
    let root = &tree.root();
    cb(WalkCallbackParams {
        direntry: root,
        parent_hash: None,
    });
    do_walk(tree, root, cb)
}

pub fn walk_diff(tree1: &dyn Tree, tree2: &dyn Tree, cb: &mut DiffWalkCallback) -> Result<(), String> {
    // We assume both trees reference the same DB.
    do_walk_diff(tree1, Some(&tree1.root()), tree2, Some(&tree2.root()), cb)
}
