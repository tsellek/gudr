pub mod fs;

use crate::consts::{Commit, ResolvedRef};
use crate::direntry::{DirEntryInfo};
use crate::hash::{EncodedHash, OwnedEncodedHash};
use crate::tree::Tree;


#[derive(Debug)]
enum DiffType {
    Added,
    Deleted,
    Modified,
}

#[derive(Debug)]
struct DiffEntry {
    pub difftype: DiffType,
    pub info: DirEntryInfo,
}

pub trait RemoteRepo {
    fn branch_get_head(&self, branch: &str) -> Result<Option<OwnedEncodedHash>, String>;
    fn commit_get(&self, encoded_commit_hash: EncodedHash) -> Result<Commit, String>;
    fn commit_list_ancestors(&self, commit: EncodedHash, since_commit: Option<EncodedHash>) -> Result<Vec<Commit>, String>;
    fn tree_get<'a>(&'a self, tree_hash: EncodedHash) -> Box<dyn Tree + 'a>;
    fn list_refs(&self) -> Result<Vec<ResolvedRef>, String>;
}
