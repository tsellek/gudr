mod tests;

use std::collections::{HashMap, HashSet, VecDeque};
use std::iter::Extend;
use std::path;
use std::process;

use diffy;
use itertools::Itertools;
use log::{trace, debug, info};
use tempfile;

use crate::config;
use crate::consts::{self, Commit, RefInfo, ResolvedRef};
use crate::db;
use crate::direntry::{DirEntry, DirEntryInfo, DirEntryType};
use crate::filesystem::Filesystem;
use crate::hash::{self, EncodedHash, OwnedEncodedHash};
use crate::repo::{self, DiffEntry, DiffType, RemoteRepo};
use crate::tree::{self, Tree};
use crate::util;

pub struct FsRepo {
    db: db::DB,
    fs: Filesystem,
    config: config::Config,
}

impl repo::RemoteRepo for FsRepo {
    fn branch_get_head(&self, branch: &str) -> Result<Option<OwnedEncodedHash>, String> {
        Ok(self.db.branch_get(branch)?.commit)
    }

    fn commit_get(&self, encoded_commit_hash: EncodedHash) -> Result<Commit, String> {
        self.db.commit_get(encoded_commit_hash)
    }

    fn commit_list_ancestors(&self, commit: hash::EncodedHash, since_commit: Option<EncodedHash>) -> Result<Vec<Commit>, String> {
        let mut cur_commit_hash = commit.to_string();
        let mut result = vec![];

        while !cur_commit_hash.is_empty() && if since_commit.is_some(){cur_commit_hash != since_commit.unwrap()} else {true} {
            let commit = self.commit_get(&&cur_commit_hash)?;
            let parent_hash = commit.parent_hash.clone();
            result.push(commit);
            cur_commit_hash = parent_hash;
        }

        Ok(result)
    }


    fn tree_get<'a>(&'a self, tree_hash: EncodedHash) -> Box<dyn Tree + 'a> {
        Box::new(tree::db::DbTree::new(&self.db, tree_hash))
    }

    fn list_refs(&self) -> Result<Vec<ResolvedRef>, String> {
        self.db.ref_list()
    }
}

impl FsRepo {
    pub fn new(path: &std::path::Path) -> Result<FsRepo, String> {
        debug!("Creating new repo at '{:?}'", &path);
        let config = config::Config::new(path)?;
        let db = db::DB::new(&config)?;
        Ok(FsRepo {
            db: db,
            fs: Filesystem::new(),
            config: config,
        })
    }

    pub fn info(&self) -> Result<String, String> {
        return Ok(format!("{}{}", self.config.info(), self.db.info()));
    }

    pub fn dump(&self) -> Result<String, String> {
        return Ok(self.db.dump());
    }

    pub fn fs_tree_store(&mut self, path: &path::Path) -> Result<hash::OwnedEncodedHash, String> {
        let fstree = tree::fs::FsTree::new(&self.fs, path);

        let mut file_info: Vec<DirEntry> = Vec::new();
        tree::walk(&fstree, &mut |params: tree::WalkCallbackParams| {
            if !self.config.should_ignore_path(&params.direntry.info.path) {
                let de: DirEntry = params.direntry.clone();
                file_info.push(de);
            }
        })?;

        let mut dir_hashes: HashMap<path::PathBuf, hash::OwnedRawHash> = HashMap::new();
        let mut toplevel_hash = vec![];

        for finfo in file_info.iter().rev() {
            //reverse so subdirs appear before their parent
            let parent_path = finfo.info.path.parent();
            match &finfo.info.detype {
                DirEntryType::File => {
                    let my_hash = self.fs.direntry_hash(&finfo)?;
                    if let Some(_) = parent_path {
                        dir_hashes
                            .entry(parent_path.unwrap().to_path_buf())
                            .or_insert(Vec::new())
                            .append(&mut my_hash.clone());
                    }
                    dir_hashes.insert(finfo.info.path.clone(), my_hash);
                }
                DirEntryType::Dir => {
                    let my_path = finfo.info.path.to_str().unwrap();
                    if !dir_hashes.contains_key(&finfo.info.path) {
                        debug!(
                            "{} is an empty directory",
                            finfo.info.path.to_string_lossy()
                        );
                    }

                    let dir_hash: &hash::OwnedRawHash =
                        dir_hashes.entry(finfo.info.path.clone()).or_default(); //If there's no hash for the dir, it means we haven't seen any of its children so it must be an empty dir.
                    let my_path_nonempty = if my_path.is_empty() { "." } else { my_path }; //Otherwise if there's a single file in the top-level dir, the dir's hash will be the same as the file
                    let my_hash = hash::hash_bytes(&[&dir_hash[..], &my_path_nonempty.as_bytes()]);
                    *dir_hashes
                        .entry(finfo.info.path.clone())
                        .or_insert(Vec::new()) = my_hash.clone();
                    if let Some(_) = parent_path {
                        dir_hashes
                            .entry(parent_path.unwrap().to_path_buf())
                            .or_insert(Vec::new())
                            .append(&mut my_hash.clone());
                    } else {
                        toplevel_hash = my_hash;
                    }
                }
            }
        }

        let mut toplevel_hash_encoded = String::new();
        for finfo in file_info.iter() {
            let file_hash = hash::encode(&dir_hashes[&finfo.info.path]);
            if let Some(_) = finfo.info.path.parent() {
                let parent_hash = hash::encode(&dir_hashes[finfo.info.path.parent().unwrap()]);
                assert_ne!(file_hash, parent_hash);
                self.db.file_insert(
                    &file_hash,
                    &finfo.info,
                    (*finfo.contents.get()).as_ref(),
                    Some(&parent_hash),
                    &hash::encode(&toplevel_hash),
                )?;
            } else {
                toplevel_hash_encoded = self.db.file_insert(
                    &file_hash,
                    &finfo.info,
                    (*finfo.contents.get()).as_ref(),
                    None,
                    &hash::encode(&toplevel_hash),
                )?;
            };
        }
        return Ok(toplevel_hash_encoded);
    }

    pub fn commit_create(
        &mut self,
        description: &str,
        user: &str,
        encoded_tree_hash: hash::EncodedHash,
        parent: hash::EncodedHash,
        merged_parent: Option<hash::EncodedHash>,
    ) -> Result<hash::OwnedEncodedHash, String> {
        let parent_hash = hash::decode(parent).unwrap();
        let commit_hash = hash::hash_bytes(&[
            &hash::decode(encoded_tree_hash).unwrap()[..],
            &parent_hash,
            &description.as_bytes(),
        ]);

        debug!("Inserting commit {}", &hash::encode(&commit_hash)[..6]);

        self.db.commit_insert(
            &hash::encode(&commit_hash),
            description,
            user,
            &encoded_tree_hash,
            &parent,
            merged_parent,
        )
    }

    pub fn commit_diff(
        &self,
        encoded_commit_hash1: hash::EncodedHash,
        encoded_commit_hash2: hash::EncodedHash,
    ) -> Result<String, String> {
        let mut result = String::new();
        self.do_commit_diff(encoded_commit_hash1, encoded_commit_hash2, &mut |params| {
            if !self.should_ignore_diff_path(params) {
                result.push_str(&FsRepo::diff_callback(params))
            }
        })?;
        Ok(result)
    }

    pub fn branch_set_head(
        &mut self,
        branch: &str,
        encoded_commit_hash: hash::EncodedHash,
    ) -> Result<String, String> {
        self.db.branch_set_head(branch, encoded_commit_hash)?;
        return Ok("".to_owned());
    }

    pub fn branch_create(&mut self, name: &str) -> Result<(), String> {
        self.db.branch_create(name, &self.current_commit()?)
    }

    pub fn current_user(&self) -> String {
        self.config.current_user()
    }

    pub fn current_branch(&self) -> &String {
        self.config.current_branch()
    }

    pub fn current_branch_set(&mut self, branch: &str) -> Result<(), String> {
        let commit = self.branch_get_head(branch)?;
        if commit.is_none() {
            return Err("Can't switch to empty branch".to_string());
        }
        let commit = commit.unwrap();
        self.config.current_branch_set(branch)?;
        self.current_commit_set(&commit)?;
        Ok(())
    }

    pub fn current_commit(&self) -> Result<hash::OwnedEncodedHash, String> {
        if let Some(commit) = self.config.current_commit() {
            Ok(commit.clone())
        } else {
            let current_branch_head = self.branch_get_head(self.config.current_branch()).unwrap();
            if let Some(branch_head) = current_branch_head {
                return Ok(branch_head);
            } else {
                return Err("Current branch has no commits".to_owned());
            }
        }
    }

    pub fn current_commit_set(&mut self, commit_hash: hash::EncodedHash) -> Result<(), String> {
        self.db.commit_get(commit_hash)?;
        self.config.current_commit_set(commit_hash);
        Ok(())
    }

    pub fn checkout(&mut self, encoded_commit_hash: hash::EncodedHash) -> Result<String, String> {
        let mut potential_direntries = vec![];
        let mut errors = vec![];

        if self.config.current_commit().is_none() {
            debug!("No current commit. Comparing with FS");
            self.get_files_to_check_out_between_commit_and_fs(
                encoded_commit_hash,
                &mut potential_direntries,
                &mut errors,
            )?;
        } else {
            self.get_files_to_check_out_between_commits(
                &self.current_commit()?,
                encoded_commit_hash,
                &mut potential_direntries,
                &mut errors,
            )?;
        }

        if !errors.is_empty() {
            return Err(errors.join("\n"));
        }

        let results = self.extract_files_from_commit(encoded_commit_hash, &potential_direntries)?;
        Ok(results.join("\n"))
    }

    pub fn tag_set(
        &mut self,
        tag_name: &str,
        encoded_commit_hash: hash::EncodedHash,
    ) -> Result<(), String> {
        self.db.tag_set(tag_name, encoded_commit_hash)
    }

    pub fn resolve_ref(&self, ref_name: &str) -> Result<consts::ResolvedRef, String> {
        if let Ok(ref_info) = self.db.tag_get(ref_name) {
            return Ok(consts::ResolvedRef::Tag(ref_info));
        } else if let Ok(ref_info) = self.db.branch_get(ref_name) {
            if ref_info.commit.is_some() {
                return Ok(consts::ResolvedRef::Branch(ref_info));
            } else {
                return Err(format!("Branch {} has no commits yet", ref_name));
            }
        }
        if ref_name.chars().all(|c| c.is_ascii_hexdigit()) {
            if let Ok(_) = self.db.commit_get(ref_name) {
                return Ok(consts::ResolvedRef::Commit(consts::RefInfo{name: ref_name.to_string(), commit: Some(ref_name.to_string()), origin: None}));
            } else {
                let partial_matches = self.db.commit_get_by_partial_hash(&ref_name)?;
                if partial_matches.len() > 1 {
                    return Err(format!(
                        "'{}' is ambiguous. Can be any of: \n{}",
                        ref_name,
                        partial_matches
                            .iter()
                            .map(|c| c.hash.clone())
                            .collect::<Vec<String>>()
                            .join("\t\n")
                    ));
                }
                if partial_matches.len() == 1 {
                    let full_hash = &partial_matches[0].hash;
                    return Ok(consts::ResolvedRef::Commit(consts::RefInfo{name: full_hash.clone(), commit: Some(full_hash.clone()), origin: None}));
                }
            }
        }
        Err(format!("'{}' not found", ref_name))
    }

    pub fn status(&self, with_diff: bool) -> Result<String, String> {
        let mut result = vec![];

        let current_commit = self.current_commit()?;
        let current_tree = self.db.commit_get(&current_commit).unwrap().tree_hash;
        let dbtree = tree::db::DbTree::new(&self.db, &current_tree);
        let fstree = tree::fs::FsTree::new(&self.fs, path::Path::new("."));

        tree::walk_diff(&dbtree, &fstree, &mut |params| {
            if self.should_ignore_diff_path(params) {
                return;
            }
            match params {
                tree::DiffWalkCallbackParams::Added(tree::WalkCallbackParams{direntry: DirEntry{info: DirEntryInfo{detype: DirEntryType::File, path, ..}, ..}, ..}) => {
                    result.push(format!("+ {}", path.to_string_lossy()));
                }
                tree::DiffWalkCallbackParams::Deleted(tree::WalkCallbackParams{direntry: DirEntry{info: DirEntryInfo{detype: DirEntryType::File, path, ..}, ..}, ..}) => {
                    result.push(format!("- {}", path.to_string_lossy()));
                }
                tree::DiffWalkCallbackParams::Diff(
                    tree::WalkCallbackParams{direntry:DirEntry{contents: file_contents1, ..},..},
                    tree::WalkCallbackParams{direntry:DirEntry{info: DirEntryInfo{detype: DirEntryType::File, path, ..}, contents: file_contents2,..},..} 
                ) => {
                    let mut diff_output = format!("~ {}", path.to_string_lossy());
                    if with_diff{
                        let file_contents1_ref = file_contents1.get().clone();
                        let file_contents2_ref = file_contents2.get().clone();
                        let file_contents1_str = std::str::from_utf8((*file_contents1_ref).as_ref());
                        let file_contents2_str = std::str::from_utf8((*file_contents2_ref).as_ref());
                        let patch_str = if file_contents1_str.is_err() || file_contents2_str.is_err() {
                            String::from("Binary file differs")
                        } else {
                            format!("\n{}", diffy::create_patch(file_contents1_str.unwrap(), file_contents2_str.unwrap()))
                        };
                        diff_output += &patch_str;
                    }

                    result.push(diff_output);
                }
                _ => {}
            }
        })?;

        Ok(result.join("\n"))
    }

    pub fn merge(&mut self, other_commit: hash::EncodedHash) -> Result<String, String> {
        let current_commit = self.current_commit()?;
        let ancestor = self.common_ancestor(&current_commit, other_commit)?;

        debug!("ancestor: {}\nother: {}\ncurrent: {}", &ancestor[..6], &other_commit[..6], &current_commit[..6]);

        let mut diff_to_current_commit = vec![];
        let mut diff_to_other_commit = vec![];
        self.do_commit_diff(&ancestor, &current_commit, &mut |params| {
            FsRepo::cb_add_diffs_to_vector(&mut diff_to_current_commit, params)
        })?;
        self.do_commit_diff(&ancestor, other_commit, &mut |params| {
            FsRepo::cb_add_diffs_to_vector(&mut diff_to_other_commit, params)
        })?;

        diff_to_current_commit.sort_by(|d1, d2| d1.info.path.cmp(&d2.info.path));
        diff_to_other_commit.sort_by(|d1, d2| d1.info.path.cmp(&d2.info.path));

        debug!("{} diffs to current commit", diff_to_current_commit.len());
        debug!("{} diffs to other commit", diff_to_other_commit.len());

        let mut merge_actions = vec![];
        let mut conflicts = vec![];

        let mut current_commit_diffentry_idx = 0;
        let mut other_commit_diffentry_idx = 0;
        while current_commit_diffentry_idx < diff_to_current_commit.len()
            && other_commit_diffentry_idx < diff_to_other_commit.len()
        {
            let current_commit_diffentry = &diff_to_current_commit[current_commit_diffentry_idx];
            let other_commit_diffentry = &diff_to_other_commit[other_commit_diffentry_idx];

            if current_commit_diffentry.info.path == other_commit_diffentry.info.path {
                current_commit_diffentry_idx += 1;
                other_commit_diffentry_idx += 1;
                let action = FsRepo::handle_merge_diff(&ancestor, other_commit, Some(current_commit_diffentry), Some(other_commit_diffentry));
                if let Err(conflict) = action {
                    conflicts.push(conflict)
                } else {
                    merge_actions.push(action.unwrap());
                }
            } else if current_commit_diffentry.info.path < other_commit_diffentry.info.path {
                current_commit_diffentry_idx += 1;
                let action = FsRepo::handle_merge_diff(&ancestor,other_commit, Some(current_commit_diffentry), None);
                if let Err(conflict) = action {
                    conflicts.push(conflict)
                } else {
                    merge_actions.push(action.unwrap());
                }
            } else {
                other_commit_diffentry_idx += 1;
                let action = FsRepo::handle_merge_diff(&ancestor, other_commit, None, Some(other_commit_diffentry));
                if let Err(conflict) = action {
                    conflicts.push(conflict)
                } else {
                    merge_actions.push(action.unwrap());
                }
            }
        }

        for current_commit_diffentry in &diff_to_current_commit[current_commit_diffentry_idx..] {
            let action = FsRepo::handle_merge_diff(&ancestor, other_commit, Some(current_commit_diffentry), None);
            if let Err(conflict) = action {
                conflicts.push(conflict)
            } else {
                merge_actions.push(action.unwrap());
            }
        }

        for other_commit_diffentry in &diff_to_other_commit[other_commit_diffentry_idx..] {
            let action = FsRepo::handle_merge_diff(&ancestor, other_commit, None, Some(other_commit_diffentry));
            if let Err(conflict) = action {
                conflicts.push(conflict)
            } else {
                merge_actions.push(action.unwrap());
            }
        }

        debug!("Executing {} actions", merge_actions.len());
        for mut action in merge_actions {
            let action_result = (*action)(self);
            if let Err(conflict) = action_result {
                conflicts.push(conflict);
            }
        }

        Ok(conflicts.join("\n"))
    }

    pub fn fetch(&mut self, remote: &dyn RemoteRepo, remote_name: &str) -> Result<Vec<ResolvedRef>, String>{
        fn ref_name<'a>(r: &ResolvedRef) -> String {r.info().name.clone()}
        fn contains(v: &Vec<ResolvedRef>, name: &String) -> bool {v.binary_search_by_key(name, |r|ref_name(r).clone()).is_ok()}
        let local_refs: Vec<ResolvedRef> = self.list_refs_from(remote_name)?.into_iter().sorted_by_key(ref_name).collect();
        let remote_refs: Vec<ResolvedRef> = remote.list_refs()?.into_iter().sorted_by_key(ref_name).collect();

        let new_refs: Vec<&ResolvedRef> = remote_refs.iter().filter(|r|{!contains(&local_refs, &ref_name(r))}).collect();
        let refs_to_update: Vec<&ResolvedRef> = remote_refs.iter().filter(|r|{contains(&local_refs, &ref_name(r))}).collect();

        debug!("New refs: {:?}", new_refs.iter().map(|r|ref_name(r)));
        debug!("Updated refs: {:?}", refs_to_update.iter().map(|r|ref_name(r)));
        let mut commits_to_fetch = HashSet::new();
        for new_ref in &new_refs {
            match new_ref {
                ResolvedRef::Tag(RefInfo{name:_, origin: _, commit: Some(commit)}) | 
                ResolvedRef::Branch(RefInfo{name:_, origin: _, commit: Some(commit)}) => {
                    let commit_ancestors = remote.commit_list_ancestors(commit, None)?.into_iter();
                    commits_to_fetch.extend(commit_ancestors);
                }
                ResolvedRef::Branch(RefInfo{name:_, origin: _, commit: None}) => {
                }
                _ => {
                    panic!("Unexpected ref");
                }
            }
        }

        for updated_ref in &refs_to_update {
            match updated_ref {
                ResolvedRef::Branch(RefInfo{name, commit: Some(commit), ..}) => {
                    let local_ref_idx = local_refs.binary_search_by_key(name, ref_name).unwrap();
                    let local_ref = &local_refs[local_ref_idx];
                    let commit_ancestors = remote.commit_list_ancestors(commit, local_ref.info().commit.as_deref())?.into_iter();
                    commits_to_fetch.extend(commit_ancestors);
                }
                ResolvedRef::Branch(RefInfo{name:_, origin: _, commit: None}) => {
                }
                _ => {
                    panic!("Unexpected ref");
                }
            }
        }

        let existing_commits: HashSet<Commit, std::collections::hash_map::RandomState> = HashSet::from_iter(self.db.commit_get_multi(commits_to_fetch.iter().map(|c|{&c.hash})).into_iter());
        commits_to_fetch = &commits_to_fetch - &existing_commits;

        for commit_to_fetch in commits_to_fetch {
            self.fetch_commit(remote, &commit_to_fetch)?;
        }

        let mut result = vec![];

        for new_ref in &new_refs {
            match new_ref {
                ResolvedRef::Branch(RefInfo{name, origin:_, commit: Some(commit)}) => {
                    trace!("Updating new remote branch {} to {}", name, &commit[..6]);
                    let resolved_ref = self.db.branch_update_remote(remote_name, name, commit)?;
                    result.push(resolved_ref);
                }
                ResolvedRef::Tag(RefInfo{name, origin:_, commit: Some(commit)}) => {
                    let resolved_ref = self.db.tag_set_remote(remote_name, name, commit)?;
                    result.push(resolved_ref);
                }
                _ => {
                    panic!("Unexpected ref");
                }
            }
        }
        for updated_ref in &refs_to_update {
            match updated_ref {
                ResolvedRef::Branch(RefInfo{name, origin:_, commit: Some(commit)}) => {
                    trace!("Updating remote branch {} to {}", name, &commit[..6]);
                    let resolved_ref = self.db.branch_update_remote(remote_name, name, commit)?;
                    result.push(resolved_ref);
                }
                _ => {
                    panic!("Unexpected ref");
                }
            }
        }

        Ok(result)
    }

    fn fetch_commit(&mut self, remote: &dyn RemoteRepo, commit: &Commit) -> Result<(), String> {
        let tree = remote.tree_get(&commit.tree_hash);
        tree::walk(tree.as_ref(), &mut |params| {self.insert_file(&commit.tree_hash, &params.direntry, params.parent_hash)})?;
        debug!("Inserting commit {} to DB", &commit.hash[..6]);
        self.db.commit_insert(&commit.hash, &commit.description, &commit.user, &commit.tree_hash, &commit.parent_hash, commit.merged_parent_hash.as_deref())?;
        Ok(())
    }

    fn insert_file<'a>(&mut self, tree_hash: EncodedHash, direntry: &DirEntry, raw_parent_hash: Option<hash::RawHash>) {
        let parent_hash = raw_parent_hash.map(|h| hash::encode(h));
        self.db.file_insert(&hash::encode(&direntry.get_hash()), &direntry.info, (*direntry.contents.get()).as_ref(), parent_hash.as_deref(), tree_hash).unwrap();
    }

    fn list_refs_from(&self, remote_repo_name: &str) -> Result<Vec<ResolvedRef>, String>{
        self.db.ref_list_from_origin(remote_repo_name)
    }

    fn common_ancestor(
        &self,
        commit1: hash::EncodedHash,
        commit2: hash::EncodedHash,
    ) -> Result<hash::OwnedEncodedHash, String> {
        let mut branch1_commit = commit1.to_owned();
        let mut branch2_commit = commit2.to_owned();

        let mut branch1_ancestors = HashSet::new();
        let mut branch2_ancestors = HashSet::new();

        while !branch1_commit.is_empty() || !branch2_commit.is_empty() {
            if !branch1_commit.is_empty() {
                if branch2_ancestors.contains(&branch1_commit) {
                    return Ok(branch1_commit.to_owned());
                }
                branch1_ancestors.insert(branch1_commit.clone());
                branch1_commit = self.db.commit_get(&branch1_commit)?.parent_hash;
            }
            if !branch2_commit.is_empty() {
                if branch1_ancestors.contains(&branch2_commit) {
                    return Ok(branch2_commit.to_owned());
                }
                branch2_ancestors.insert(branch2_commit.clone());
                branch2_commit = self.db.commit_get(&branch2_commit)?.parent_hash;
            }
        }

        panic!("Two branches don't share an ancestor");
    }

    fn handle_merge_diff<'a>(
        ancestor_commit: hash::EncodedHash<'a>,
        other_commit: hash::EncodedHash<'a>,
        current_commit_diff: Option<&'a DiffEntry>,
        other_commit_diff: Option<&'a DiffEntry>,
    ) -> Result<Box<dyn FnMut(&mut FsRepo) -> Result<(), String> + 'a>, String> {
        trace!("Diff: {:?}/{:?}", current_commit_diff, other_commit_diff);
        match (current_commit_diff, other_commit_diff) {
            // If something changed in the current commit and not in the other, we can just leave things as they are
            (Some(DiffEntry{difftype: DiffType::Added,info: _}), None) => Ok(Box::new(|_| Ok(()))),
            (Some(DiffEntry{difftype: DiffType::Deleted, info: _}),None) => Ok(Box::new(|_| Ok(()))),
            (Some(DiffEntry{difftype: DiffType::Modified, info: _}),None) => Ok(Box::new(|_| Ok(()))),

            // If something changed in the other commit and not in the current one, we need to replicate the change
            (None, Some(DiffEntry{difftype: DiffType::Added, info})) => {
                if info.detype == DirEntryType::File {
                    Ok(Box::new(|repo| repo.extract_file(other_commit, &info.path)))
                } else {
                    Ok(Box::new(|repo| repo.fs.create_dir(&info.path)))
                }
            }
            (None, Some(DiffEntry{difftype: DiffType::Deleted, info})) => Ok(Box::new(|repo| repo.fs.remove_file(&info.path))),
            (None, Some(DiffEntry{difftype: DiffType::Modified,info})) => Ok(Box::new(|repo| repo.extract_file(other_commit, &info.path))),

            //if something changed in both commits, things get interesting...
            (Some(DiffEntry{difftype: DiffType::Added,info: info1}), Some(DiffEntry{difftype: DiffType::Added, info: _})) => 
                Ok(Box::new(|repo| repo.merge_with_current_commit(ancestor_commit, other_commit, &info1.path))),
            (Some(DiffEntry{difftype: DiffType::Added,info: _}), Some(DiffEntry{difftype: DiffType::Deleted, info: _})) => {
                // If it was added, that means it didn't exist. But then how can it be deleted?!
                panic!("File was added in one commit and deleted in the other?!");
            }
            (Some(DiffEntry{difftype: DiffType::Added, info: _}), Some(DiffEntry{difftype: DiffType::Modified, info: _})) => {
                // If it was added, that means it didn't exist. But then how can it be modified?!
                panic!("File was added in one commit and modifier in the other?!");
            }
            (Some(DiffEntry{difftype: DiffType::Deleted, info: _}), Some(DiffEntry{difftype: DiffType::Added, info: _})) => {
                // If it was added, that means it didn't exist. But then how can it be deleted?!
                panic!("File was added in one commit and deleted in the other?!");
            }
            (Some(DiffEntry{difftype: DiffType::Deleted, info: _}), Some(DiffEntry{difftype: DiffType::Deleted, info: _})) => {
                // Deleted in both. No need to do anything.
                Ok(Box::new(|_| Ok(())))
            }
            (Some(DiffEntry{difftype: DiffType::Deleted, info: info1}), Some(DiffEntry{difftype: DiffType::Modified, info: _})) => 
                Err(format!("'{}' was deleted in the current commit, modified in the commit to be merged", info1.path.to_string_lossy())),
            (Some(DiffEntry{difftype: DiffType::Modified, info: _}), Some(DiffEntry{difftype: DiffType::Added, info: _})) => {
                // If it was added, that means it didn't exist. But then how can it be modified?!
                panic!("File was added in one commit and modifier in the other?!");
            }
            (Some(DiffEntry{difftype: DiffType::Modified, info: info1}), Some(DiffEntry{difftype: DiffType::Deleted, info: _})) => 
                Err(format!("'{}' was modified in the current commit, deleted in the commit to be merged",info1.path.to_string_lossy())),
            (Some(DiffEntry{difftype: DiffType::Modified, info: info1}), Some(DiffEntry{difftype: DiffType::Modified, info: _})) => 
                Ok(Box::new(|repo| {repo.merge_with_current_commit(ancestor_commit, other_commit, &info1.path)})),
            (None, None) => {
                panic!("No diff on either side of merge?!");
            }
        }
    }

    fn extract_file(&mut self, commit: hash::EncodedHash, path: &path::PathBuf) -> Result<(), String> {
        let tree_hash = self.db.commit_get(commit).unwrap().tree_hash;
        let file = self.db.file_get_by_tree_and_path(&tree_hash, &path.to_string_lossy()).unwrap();
        if file.info.detype == DirEntryType::File {
            trace!("Writing file {:?}", path);
            self.fs.write_file(path, (*file.contents.get()).as_ref())
        } else {
            Ok(())
        }
    }

    fn merge_with_current_commit(
        &mut self,
        ancestor_commit: hash::EncodedHash,
        commit: hash::EncodedHash,
        path: &path::PathBuf,
    ) -> Result<(), String> {
        {
            let tree_hash = self.db.commit_get(commit).unwrap().tree_hash;
            let file = self.db.file_get_by_tree_and_path(&tree_hash, &path.to_string_lossy()).unwrap();
            if file.info.detype != DirEntryType::File {
                return Ok(());
            }
        }

        let baseline_file = util::fs::op(tempfile::NamedTempFile::new()).unwrap();
        let to_be_merged_file = util::fs::op(tempfile::NamedTempFile::new()).unwrap();
        let current_file = util::fs::op(tempfile::NamedTempFile::new()).unwrap();
        self.extract_file_from_commit_to(ancestor_commit, path, baseline_file.path()).ok(); // if the file did not exist in the ancestor commit, it must have been added in the other two commits.
        self.extract_file_from_commit_to(commit, path, to_be_merged_file.path()).unwrap();
        self.extract_file_from_commit_to(&self.current_commit().unwrap(), path, current_file.path()).unwrap();

        let mut diff_command = process::Command::new("diff3");
        diff_command.arg("-m")
            .arg("-L").arg("Common Ancestor")
            .arg("-L").arg("To be merged")
            .arg("-L").arg("Current")
            .arg(baseline_file.path())
            .arg(to_be_merged_file.path())
            .arg(current_file.path());

        
        trace!("{:?}", diff_command);
        let diff_output = util::fs::op(diff_command.output()).unwrap();
        trace!("{}", String::from_utf8_lossy(&diff_output.stdout));
        if diff_output.status.code().is_none() || diff_output.status.code().unwrap() >= 2 {
            panic!("Failed to execute diff program: [{}] {}", diff_output.status, String::from_utf8_lossy(&diff_output.stderr));
        }
        self.fs.write_file(path, &diff_output.stdout).unwrap();
        if diff_output.status.code().unwrap() == 1 {
            return Err(format!("Conflict in {}", path.to_string_lossy()));
        }
        Ok(())
    }

    fn cb_add_diffs_to_vector(results: &mut Vec<DiffEntry>, params: &tree::DiffWalkCallbackParams) {
        match params {
            tree::DiffWalkCallbackParams::Added(tree::WalkCallbackParams{direntry, ..}) => {
                results.push(DiffEntry {
                    difftype: DiffType::Added,
                    info: direntry.info.clone(),
                });
            }
            tree::DiffWalkCallbackParams::Deleted(tree::WalkCallbackParams{direntry, ..}) => {
                results.push(DiffEntry {
                    difftype: DiffType::Deleted,
                    info: direntry.info.clone(),
                });
            }
            tree::DiffWalkCallbackParams::Diff(_, tree::WalkCallbackParams{direntry: direntry2, ..}) => {
                results.push(DiffEntry {
                    difftype: DiffType::Modified,
                    info: direntry2.info.clone(),
                });
            }
        }
    }

    fn extract_file_from_commit_to(
        &mut self,
        commit_hash: hash::EncodedHash,
        file_path: &path::Path,
        dest_file_path: &path::Path,
    ) -> Result<(), String> {
        let tree_hash = self.db.commit_get(&commit_hash)?.tree_hash;
        let file = self.db.file_get_by_tree_and_path(&tree_hash, &file_path.to_string_lossy())?;
        if file.info.detype != DirEntryType::File {
            return Err(format!(
                "'{}' is not a file",
                file.info.path.to_string_lossy()
            ));
        }

        self.fs.write_file(&dest_file_path, (*file.contents.get()).as_ref())
    }

    fn extract_files_from_commit(
        &mut self,
        commit_hash: hash::EncodedHash,
        paths: &[path::PathBuf],
    ) -> Result<Vec<String>, String> {
        let mut results = vec![];
        let tree_hash = self.db.commit_get(&commit_hash).unwrap().tree_hash;

        let base_path = path::PathBuf::from(&self.config.path);
        for path in paths {
            let file = self
                .db
                .file_get_by_tree_and_path(&tree_hash, &path.to_string_lossy());
            if let Err(_) = file {
                // since we're sure the list of paths is valid, this must mean the file was deleted in the new commit
                if path.is_file() {
                    info!("Deleting file '{}'", path.to_string_lossy());
                    self.fs.remove_file(&path)?;
                    results.push(format!("Deleted file '{}'", path.to_string_lossy()));
                }
                continue;
            }
            let file = file.unwrap();
            let path_to_write = base_path.join(&file.info.path);
            if file.info.detype == DirEntryType::Dir {
                if !file.info.path.to_string_lossy().is_empty() && !path_to_write.exists() {
                    info!("Creating directory '{}'", &path_to_write.to_string_lossy());
                    self.fs.create_dir(&path_to_write)?;
                    results.push(format!("Created directory '{}'", path.to_string_lossy()));
                }
            } else if file.info.detype == DirEntryType::File {
                info!("Writing file '{}'", &path_to_write.to_string_lossy());
                self.fs.write_file(&path_to_write, (*file.contents.get()).as_ref())?;
                results.push(format!("Wrote file '{}'", path.to_string_lossy()));
            }
        }
        Ok(results)
    }

    fn get_files_to_check_out_between_commit_and_fs(
        &self,
        commit_hash: hash::EncodedHash,
        results: &mut Vec<path::PathBuf>,
        errors: &mut Vec<String>,
    ) -> Result<(), String> {
        let tree_hash = self.db.commit_get(&commit_hash).unwrap().tree_hash;
        let mut files =
            VecDeque::from([self.db.file_get_by_tree_and_path(&tree_hash, "").unwrap()]);
        debug!("Reading files from commit {}, tree {}", &commit_hash[..6], &tree_hash[..6]);
        while !files.is_empty() {
            let direntry = files.pop_front().unwrap();
            trace!("Direntry: hash={}, path={:?}, type={:?} ", &hex::encode(direntry.get_hash())[..6], &direntry.info.path, direntry.info.detype);
            match direntry.info.detype {
                DirEntryType::Dir => {
                    results.push(direntry.info.path.clone());
                    let children_iter = self
                        .db
                        .file_get_children(&hash::encode(&direntry.get_hash()))
                        .unwrap()
                        .into_iter();
                    files.append(&mut VecDeque::from_iter(children_iter));
                }
                DirEntryType::File => {
                    if direntry.info.path.exists() {
                        trace!("File {:?} exists in the filesystem.", &direntry.info.path);
                        let file_hash_from_filesystem = self.fs.direntry_hash(&direntry)?;
                        let file_hash_from_commit = direntry.get_hash();
                        if file_hash_from_commit != file_hash_from_filesystem {
                            errors.push(format!(
                                "File '{}' exists on the filesystem and is different from commit",
                                direntry.info.path.to_string_lossy()
                            ))
                        }
                    } else {
                        debug!("File {:?} is not on the filesystem. Will check out.", &direntry.info.path);
                        results.push(direntry.info.path);
                    }
                }
            }
        }
        Ok(())
    }

    fn get_files_to_check_out_between_commits(
        &self,
        commit_hash1: hash::EncodedHash,
        commit_hash2: hash::EncodedHash,
        potential_direntries: &mut Vec<path::PathBuf>,
        errors: &mut Vec<String>,
    ) -> Result<(), String> {
        self.do_commit_diff(commit_hash1, commit_hash2, &mut |params| {
            if !self.should_ignore_diff_path(params) {
                match params {
                    tree::DiffWalkCallbackParams::Added(tree::WalkCallbackParams{direntry: DirEntry{info: DirEntryInfo{detype: DirEntryType::File, path, ..}, ..}, ..})  => 
                    {
                        if let Err(errstr) = self.should_overwrite_added(path) {
                            errors.push(errstr);
                        } else {
                            potential_direntries.push(path.clone());
                        }
                    }
                    tree::DiffWalkCallbackParams::Deleted(tree::WalkCallbackParams{direntry: DirEntry{info: DirEntryInfo{detype: DirEntryType::File, path, ..}, ..}, ..}) => {
                        if let Err(errstr) = self.should_overwrite_deleted(path) {
                            errors.push(errstr);
                        } else {
                            potential_direntries.push(path.clone());
                        }
                    }
                    tree::DiffWalkCallbackParams::Diff(
                        tree::WalkCallbackParams{direntry: DirEntry{info: DirEntryInfo{detype: DirEntryType::File, path, ..}, ..}, ..},
                        _
                    ) => {
                        if let Err(errstr) = self.should_overwrite_modified(path) {
                            errors.push(errstr);
                        } else {
                            potential_direntries.push(path.clone());
                        }
                    }
                    tree::DiffWalkCallbackParams::Diff(
                        _,
                        tree::WalkCallbackParams{direntry: DirEntry{info: DirEntryInfo{detype: DirEntryType::File, path, ..}, ..}, ..}
                    ) => {
                        if let Err(errstr) = self.should_overwrite_modified(path) {
                            errors.push(errstr);
                        } else {
                            potential_direntries.push(path.clone());
                        }
                    }
                    _ => {}
                }
            }
        })?;
        Ok(())
    }

    fn should_overwrite_added(&self, path: &path::PathBuf) -> Result<(), String> {
        if path.exists() {
            return Err(format!(
                "The file '{}' exists in the file system but was added in the commit",
                path.to_string_lossy()
            ));
        }
        Ok(())
    }

    fn should_overwrite_deleted(&self, path: &path::PathBuf) -> Result<(), String> {
        if !path.exists() {
            return Ok(());
        }

        let file_hash_from_commit = match self.get_file_from_current_commit_by_path(path) {
            Ok(direntry) => direntry.get_hash(),
            Err(_) => {
                vec![]
            }
        };

        if file_hash_from_commit.is_empty() {
            panic!("Deleted file did not exist in original commit?!");
        }
        let file_hash_from_filesystem = self.fs.file_hash(path)?;

        if file_hash_from_commit != file_hash_from_filesystem {
            return Err(format!(
                "The file {} was modified on the file system but deleted in the commit",
                path.to_string_lossy()
            ));
        }
        Ok(())
    }

    fn should_overwrite_modified(&self, path: &path::PathBuf) -> Result<(), String> {
        if !path.exists() {
            return Err(format!(
                "The file {} was deleted on the file system and modified in the commit",
                path.to_string_lossy()
            ));
        }

        let file_hash_from_commit = self.get_file_from_current_commit_by_path(path)?.get_hash();
        let file_hash_from_filesystem = self.fs.file_hash(path)?;

        if file_hash_from_commit != file_hash_from_filesystem {
            return Err(format!(
                "The file {} was modified on the file system and modified in the commit",
                path.to_string_lossy()
            ));
        }
        Ok(())
    }

    fn get_file_from_current_commit_by_path(
        &self,
        path: &path::PathBuf,
    ) -> Result<DirEntry, String> {
        let current_commit = self
            .commit_get(&self.config.current_commit().unwrap())
            .unwrap();
        let result = self
            .db
            .file_get_by_tree_and_path(&current_commit.tree_hash, &path.to_string_lossy());
        if let Err(ref errstr) = result {
            info!(
                "Path {} does not exist in commit {}: {}",
                path.to_string_lossy(),
                current_commit.hash,
                errstr
            );
        }
        result
    }

    fn do_commit_diff(
        &self,
        encoded_commit_hash1: hash::EncodedHash,
        encoded_commit_hash2: hash::EncodedHash,
        cb: &mut tree::DiffWalkCallback,
    ) -> Result<(), String> {
        let tree1_hash = self.db.commit_get(encoded_commit_hash1).unwrap().tree_hash;
        let tree2_hash = self.db.commit_get(encoded_commit_hash2).unwrap().tree_hash;
        tree::walk_diff(
            &tree::db::DbTree::new(&self.db, &tree1_hash),
            &tree::db::DbTree::new(&self.db, &tree2_hash),
            cb,
        )
    }

    fn should_ignore_diff_path(&self, params: &tree::DiffWalkCallbackParams) -> bool {
        match params {
            tree::DiffWalkCallbackParams::Added(tree::WalkCallbackParams{direntry: DirEntry{info: DirEntryInfo{path, ..}, ..}, ..}) => {
                self.config.should_ignore_path(&path)
            }
            tree::DiffWalkCallbackParams::Deleted(tree::WalkCallbackParams{direntry: DirEntry{info: DirEntryInfo{path, ..}, ..}, ..}) => {
                self.config.should_ignore_path(&path)
            }
            tree::DiffWalkCallbackParams::Diff(tree::WalkCallbackParams{direntry: DirEntry{info: DirEntryInfo{path: path1, ..}, ..}, ..}, _) => {
                self.config.should_ignore_path(&path1)
            }
        }
    }

    fn diff_callback(params: &tree::DiffWalkCallbackParams) -> String {
        match params {
            tree::DiffWalkCallbackParams::Added(tree::WalkCallbackParams{direntry: DirEntry{info: DirEntryInfo{path, ..}, ..}, ..}) => {
                format!("+ {}\n", path.to_string_lossy())
            }
            tree::DiffWalkCallbackParams::Deleted(tree::WalkCallbackParams{direntry: DirEntry{info: DirEntryInfo{path, ..}, ..}, ..}) => {
                format!("- {}\n", path.to_string_lossy())
            }
            tree::DiffWalkCallbackParams::Diff(
                tree::WalkCallbackParams{direntry: DirEntry{info: DirEntryInfo{detype: DirEntryType::File, path, ..}, contents: file_contents1,.. }, ..},
                tree::WalkCallbackParams{direntry: DirEntry{info: DirEntryInfo{..}, contents: file_contents2,.. }, ..},
            ) => {
                let file_contents1_ref = file_contents1.get().clone();
                let file_contents2_ref = file_contents2.get().clone();
                let file_contents1_str = std::str::from_utf8((*file_contents1_ref).as_ref()).unwrap();
                let file_contents2_str = std::str::from_utf8((*file_contents2_ref).as_ref()).unwrap();
                let diff = format!("\n{}", diffy::create_patch(file_contents1_str, file_contents2_str));
                format!("~ {}\n{}", path.to_string_lossy(), diff)
            }
            tree::DiffWalkCallbackParams::Diff(
                tree::WalkCallbackParams{direntry: DirEntry{info: DirEntryInfo{detype: DirEntryType::Dir, path, ..},.. }, ..},
                tree::WalkCallbackParams{direntry: DirEntry{info: DirEntryInfo{..},.. }, ..},
            ) => {
                format!("~ {}\n", path.to_string_lossy())
            }
        }
    }
}

impl Drop for FsRepo {
    fn drop(&mut self) {
        self.config.save();
    }
}