#[cfg(test)]
mod run_tests {

    use std::fs;
    use std::path;

    use ::function_name::named;
    use insta;

    use crate::consts::{RefInfo, ResolvedRef};
    use crate::hash::{OwnedEncodedHash};
    use crate::repo::RemoteRepo;
    use crate::repo::fs::FsRepo;
    use crate::util::test;

    fn enable_logging() {
        let _ = env_logger::builder().is_test(true).try_init();
    }
    
    #[test]
    #[named]
    fn test_repo_create() {
        let repo = create_test_repo(function_name!());

        insta::assert_debug_snapshot!(repo.info().unwrap());
    }

    #[test]
    #[named]
    fn test_repo_common_ancestor_same_commit() {
        let mut repo = create_test_repo(function_name!());
        let base_commit = commit_current_fs_tree(&mut repo, "base commit");
        assert_eq!(
            base_commit,
            repo.common_ancestor(&base_commit, &base_commit).unwrap()
        );
    }

    #[test]
    #[named]
    fn test_repo_common_ancestor_same_length() {
        let mut repo = create_test_repo(function_name!());
        let path = repo.config.path.clone();
        let base_commit = commit_current_fs_tree(&mut repo, "base commit");
        repo.branch_create("branch1").unwrap();
        repo.current_branch_set("branch1").unwrap();
        fs::write(format!("{}/file1.txt", path), "some_data").unwrap();
        let commit1 = commit_current_fs_tree(&mut repo, "commit 1");
        repo.current_branch_set("trunk").unwrap();
        fs::write(format!("{}/file2.txt", path), "some_data_2").unwrap();
        let commit2 = commit_current_fs_tree(&mut repo, "commit 2");
        assert_eq!(
            base_commit,
            repo.common_ancestor(&commit1, &commit2).unwrap()
        );
    }

    #[test]
    #[named]
    fn test_repo_common_ancestor_different_length() {
        let mut repo = create_test_repo(function_name!());
        let path = repo.config.path.clone();
        let base_commit = commit_current_fs_tree(&mut repo, "base commit");
        repo.branch_create("branch1").unwrap();
        repo.branch_create("branch2").unwrap();
        repo.current_branch_set("branch1").unwrap();
        fs::write(format!("{}/file1.txt", path), "some_data").unwrap();
        let commit1 = commit_current_fs_tree(&mut repo, "commit 1");
        repo.current_branch_set("branch2").unwrap();
        for i in 1..10 {
            fs::write(format!("{}/file{}.txt", path, i), "some_data_2").unwrap();
            commit_current_fs_tree(&mut repo, "commit 2");
        }
        let commit2 = repo.branch_get_head("branch2").unwrap().unwrap();
        assert_eq!(
            base_commit,
            repo.common_ancestor(&commit1, &commit2).unwrap()
        );
    }

    #[test]
    #[named]
    fn test_regular_commit_has_no_merged_parent() {
        let mut repo = create_test_repo(function_name!());
        let path = repo.config.path.clone();
        commit_current_fs_tree(&mut repo, "base commit");
        fs::write(format!("{}/file1.txt", path), "some_data").unwrap();
        let commit = commit_current_fs_tree(&mut repo, "commit 1");
        assert!(repo
            .commit_get(&commit)
            .unwrap()
            .merged_parent_hash
            .is_none());
    }


    #[test]
    #[named]
    fn test_ref_list_new_repo() {
        let repo = create_test_repo(function_name!());
        let refs = repo.list_refs().unwrap();
        assert_eq!(refs.len(), 1);
        assert_eq!(refs[0], ResolvedRef::Branch(RefInfo{name: "trunk".to_string(), origin: None, commit: None}));
    }


    #[test]
    #[named]
    fn test_commit_ancestors_single_commit() {
        let mut repo = create_test_repo(function_name!());

        let commit = commit_current_fs_tree(&mut repo, "base commit");

        insta::assert_debug_snapshot!(repo.commit_list_ancestors(&commit, None).unwrap());
    }

    #[test]
    #[named]
    fn test_commit_ancestors_multiple_commits() {
        let mut repo = create_test_repo(function_name!());
        let path = repo.config.path.clone();

        for i in 0..10 {
            fs::write(format!("{}/file{}.txt", path, i), "some_data").unwrap();
            commit_current_fs_tree(&mut repo, &format!("commit {}", i));
        }

        fs::write(format!("{}/last_file.txt", path), "some_data").unwrap();
        let commit = commit_current_fs_tree(&mut repo, "last commit");

        insta::assert_debug_snapshot!(repo.commit_list_ancestors(&commit, None).unwrap());
    }

    #[test]
    #[named]
    fn test_commit_ancestors_multiple_commits_with_cutoff() {
        let mut repo = create_test_repo(function_name!());
        let path = repo.config.path.clone();

        for i in 0..5 {
            fs::write(format!("{}/file{}.txt", path, i), "some_data").unwrap();
            commit_current_fs_tree(&mut repo, &format!("commit {}", i));
        }

        fs::write(format!("{}/file5.txt", path), "some_data").unwrap();
        let cutoff_commit = commit_current_fs_tree(&mut repo, "cutoff commit");

        for i in 6..10 {
            fs::write(format!("{}/file{}.txt", path, i), "some_data").unwrap();
            commit_current_fs_tree(&mut repo, &format!("commit {}", i));
        }

        fs::write(format!("{}/last_file.txt", path), "some_data").unwrap();
        let commit = commit_current_fs_tree(&mut repo, "last commit");

        insta::assert_debug_snapshot!(repo.commit_list_ancestors(&commit, Some(&cutoff_commit)).unwrap());
    }

    #[test]
    #[named]
    fn test_ref_list_tag() {
        let mut repo = create_test_repo(function_name!());

        let commit = commit_current_fs_tree(&mut repo, "base commit");
        repo.tag_set("test_tag", &commit).unwrap();
        
        let refs = repo.list_refs().unwrap();
        assert_eq!(refs.len(), 2);
        assert_eq!(refs[1], ResolvedRef::Tag(RefInfo{name: "test_tag".to_string(), origin: None, commit: Some(commit)}));
    }

    #[test]
    #[named]
    fn test_ref_list_many_refs() {
        const NREFS: usize = 100;
        let mut repo = create_test_repo(function_name!());

        let commit = commit_current_fs_tree(&mut repo, "base commit");

        for i in 0..NREFS {
            repo.tag_set(&format!("test_tag_{}", i), &commit).unwrap();
            repo.branch_create(&format!("test_branch_{}", i)).unwrap();
        }
        
        let refs = repo.list_refs().unwrap();
        assert_eq!(refs.len(), 2*NREFS+1);
        
        for i in 0..NREFS {
            assert!(matches!(refs[2*i+1], ResolvedRef::Tag(_)));
            assert_eq!(refs[2*i+1].info().name, format!("test_tag_{}", i));
            assert!(matches!(refs[2*i+2], ResolvedRef::Branch(_)));
            assert_eq!(refs[2*i+2].info().name, format!("test_branch_{}", i));
        }
    }


    #[test]
    #[named]
    fn test_fetch_empty_commit() {
        const REMOTE_COMMIT_DESC: &str = "remote_commit";
        let test_dir = test::prepare_test_dir(file!(), function_name!());
        let mut local_repo = create_test_sub_repo(&test_dir, "local_repo");
        let mut remote_repo = create_test_sub_repo(&test_dir, "remote_repo");

        let commit_hash = commit_current_fs_tree(&mut remote_repo, REMOTE_COMMIT_DESC);

        assert!(local_repo.commit_get(&commit_hash).is_err());

        local_repo.fetch_commit(&remote_repo, &remote_repo.commit_get(&commit_hash).unwrap()).unwrap();

        assert_eq!(REMOTE_COMMIT_DESC, &local_repo.commit_get(&commit_hash).unwrap().description);
        local_repo.checkout(&commit_hash).unwrap();
    }

    #[test]
    #[named]
    fn test_fetch_commit_with_file() {
        enable_logging();
        const REMOTE_COMMIT_DESC: &str = "remote_commit";
        let test_dir = test::prepare_test_dir(file!(), function_name!());
        let mut local_repo = create_test_sub_repo(&test_dir, "local_repo");
        let mut remote_repo = create_test_sub_repo(&test_dir, "remote_repo");

        fs::write(format!("{}/{}/test_file.txt", test_dir, "remote_repo"), "some_contents").unwrap();

        let commit_hash = commit_current_fs_tree(&mut remote_repo, REMOTE_COMMIT_DESC);

        assert!(local_repo.commit_get(&commit_hash).is_err());

        local_repo.fetch_commit(&remote_repo, &remote_repo.commit_get(&commit_hash).unwrap()).unwrap();

        assert_eq!(REMOTE_COMMIT_DESC, &local_repo.commit_get(&commit_hash).unwrap().description);

        assert!(!path::Path::new(&format!("{}/{}/test_file.txt", test_dir, "local_repo")).exists());
        insta::assert_debug_snapshot!(local_repo.checkout(&commit_hash).unwrap());
        assert_eq!("some_contents", fs::read_to_string(&path::Path::new(&format!("{}/{}/test_file.txt", test_dir, "local_repo"))).unwrap());
    }

    #[test]
    #[named]
    fn test_fetch_new_branch() {
        const REMOTE_BRANCH_NAME: &str = "remote_branch";
        let test_dir = test::prepare_test_dir(file!(), function_name!());
        let mut local_repo = create_test_sub_repo(&test_dir, "local_repo");
        let mut remote_repo = create_test_sub_repo(&test_dir, "remote_repo");

        fs::write(format!("{}/{}/test_file.txt", test_dir, "remote_repo"), "some_contents").unwrap();

        let commit = commit_current_fs_tree(&mut remote_repo, "remote commit");
        remote_repo.branch_create(REMOTE_BRANCH_NAME).unwrap();

        assert!(local_repo.branch_get_head(REMOTE_BRANCH_NAME).is_err());

        local_repo.fetch(&remote_repo, "remote_repo").unwrap();

        assert_eq!(commit, local_repo.branch_get_head(REMOTE_BRANCH_NAME).unwrap().unwrap());
        assert!(!path::Path::new(&format!("{}/{}/test_file.txt", test_dir, "local_repo")).exists());
        let local_commit_hash = local_repo.resolve_ref(REMOTE_BRANCH_NAME).unwrap().info().commit.clone().unwrap();
        assert_eq!(local_commit_hash, commit);
        insta::assert_debug_snapshot!(local_repo.checkout(&local_commit_hash).unwrap());
        assert_eq!("some_contents", fs::read_to_string(&path::Path::new(&format!("{}/{}/test_file.txt", test_dir, "local_repo"))).unwrap());
    }

    #[test]
    #[named]
    fn test_fetch_new_tag() {
        const REMOTE_TAG_NAME: &str = "remote_tag";
        let test_dir = test::prepare_test_dir(file!(), function_name!());
        let mut local_repo = create_test_sub_repo(&test_dir, "local_repo");
        let mut remote_repo = create_test_sub_repo(&test_dir, "remote_repo");

        fs::write(format!("{}/{}/test_file.txt", test_dir, "remote_repo"), "some_contents").unwrap();

        let commit = commit_current_fs_tree(&mut remote_repo, "remote commit");
        remote_repo.tag_set(REMOTE_TAG_NAME, &commit).unwrap();

        assert!(local_repo.resolve_ref(REMOTE_TAG_NAME).is_err());

        local_repo.fetch(&remote_repo, "remote_repo").unwrap();

        let local_commit_hash = local_repo.resolve_ref(REMOTE_TAG_NAME).unwrap().info().commit.clone().unwrap();
        assert_eq!(commit, local_commit_hash);
        assert!(!path::Path::new(&format!("{}/{}/test_file.txt", test_dir, "local_repo")).exists());
        insta::assert_debug_snapshot!(local_repo.checkout(&local_commit_hash).unwrap());
        assert_eq!("some_contents", fs::read_to_string(&path::Path::new(&format!("{}/{}/test_file.txt", test_dir, "local_repo"))).unwrap());
    }


    #[test]
    #[named]
    fn test_fetch_updated_branch() {
        const REMOTE_BRANCH_NAME: &str = "remote_branch";
        let test_dir = test::prepare_test_dir(file!(), function_name!());
        let mut local_repo = create_test_sub_repo(&test_dir, "local_repo");
        let mut remote_repo = create_test_sub_repo(&test_dir, "remote_repo");

        commit_current_fs_tree(&mut remote_repo, "remote_commit1");
        remote_repo.branch_create(REMOTE_BRANCH_NAME).unwrap();
        remote_repo.current_branch_set(REMOTE_BRANCH_NAME).unwrap();

        local_repo.fetch(&remote_repo, "remote_repo").unwrap();

        fs::write(format!("{}/{}/test_file.txt", test_dir, "remote_repo"), "some_contents").unwrap();
        let commit = commit_current_fs_tree(&mut remote_repo, "remote commit2");

        local_repo.fetch(&remote_repo, "remote_repo").unwrap();

        assert_eq!(commit, local_repo.branch_get_head(REMOTE_BRANCH_NAME).unwrap().unwrap());
        assert!(!path::Path::new(&format!("{}/{}/test_file.txt", test_dir, "local_repo")).exists());
        insta::assert_debug_snapshot!(local_repo.checkout(&commit).unwrap());
        assert_eq!("some_contents", fs::read_to_string(&path::Path::new(&format!("{}/{}/test_file.txt", test_dir, "local_repo"))).unwrap());
    }

    fn create_test_repo(test_name: &str) -> FsRepo {
        let path = test::get_test_dir(file!(), test_name);

        let mut gudr_path = path::PathBuf::from(&path);
        gudr_path.push(".gudr");
        fs::create_dir_all(gudr_path).unwrap();

        return FsRepo::new(path::Path::new(&path)).unwrap();
    }

    fn create_test_sub_repo(test_dir: &str, repo_name: &str) -> FsRepo {
        let mut repo_path = path::PathBuf::from(&test_dir);
        repo_path.push(repo_name);

        let mut gudr_path = path::PathBuf::from(&repo_path);
        gudr_path.push(".gudr");
        fs::create_dir_all(gudr_path).unwrap();

        return FsRepo::new(&repo_path).unwrap();
    }

    fn commit_current_fs_tree(repo: &mut FsRepo, desc: &str) -> OwnedEncodedHash {
        let current_branch_head = repo.branch_get_head(repo.current_branch()).unwrap();
        let current_branch_head_encoded = if let Some(branch_head) = current_branch_head {
            branch_head
        } else {
            String::new()
        };
        let path = repo.config.path.clone();
        let tree_hash = repo.fs_tree_store(path::Path::new(&path)).unwrap();
        let commit_hash = repo.commit_create(desc,
                                             &repo.current_user(),
                                             &tree_hash,
                                             &current_branch_head_encoded,
                                             None,
                                            ).unwrap();
        let current_branch = repo.current_branch().clone();
        repo.branch_set_head(&current_branch, &commit_hash).unwrap();
        repo.current_commit_set(&commit_hash).unwrap();
        return commit_hash;
    }
}
